<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="ar">
<!--<![endif]-->
<head>

<!-- Basic Page Needs -->
<meta charset="utf-8" />
<title>اختبار Strong لتحديد الميول</title>
<meta name="description" content="" />
<meta name="author" content="Mohamad Al Jasem" />

<!-- Favicons-->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- CSS -->
<link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="css/socialize-bookmarks.css" rel="stylesheet" />

<!-- Jquery -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.8.12.min.js"></script>

<!-- Wizard-->
<script src="js/jquery.wizard.js"></script>


 <script class="rs-file" src="royalslider/jquery.royalslider.min.js"></script>
    <link class="rs-file" href="royalslider/royalslider.css" rel="stylesheet">
    
    <!-- slider stylesheets -->
      <link class="rs-file" href="royalslider/skins/default/rs-default.css" rel="stylesheet">
     <!-- Chart JS -->
	 <script src="js/Chart.js"></script>

    

    
    <!-- slider css -->
    <style>
      .contentSlider {
  width: 100%;
}
.contentSlider,
.contentSlider .rsOverflow,
.contentSlider .rsSlide,
.contentSlider .rsVideoFrameHolder,
.contentSlider .rsThumbs {
  background: #eee;
  color: #000;
}

.contentSlider .rsSlide,
.contentSlider .rsOverflow {
  background: #eee;
}
.contentSlider h3 {
  font-size: 24px;
  line-height: 31px;
  margin: 12px 0 8px;
  font-weight: bold;
}
.contentSlider img {
  max-width: 100%;
  height: auto;
  display: block;
}
.content-slider-bg {

  padding: 24px 7%;
  background: #eee;
}
#page-navigation { display: none; }
table
{
	width:100%;
}
    </style> 

<!-- HTML5 and CSS3-in older browsers-->
<script src="js/modernizr.custom.17475.js"></script>

<!-- Support media queries for IE8 -->
<script src="js/respond.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<header>
         <div class="container">
            <div class="row">
			<div id="logo">
			<a href="index.html" ><img src="img/du.png" alt="شعار جامعة دمشق" /></a>
			</div>
			<div id="s_t">
				<h1>جامعة دمشق</h1>
				</br>
				<h3>مركز التوجيه المهني</h3>
			</div>
			<div>
		<div id="m_m">
		
			<img src="img/logo2.png" style="float:left; width:228px; height:120px;">
		</div>
		</div>
		
         </div><!-- End row -->
         </div><!-- End container -->
        </header> <!-- End header -->
        	
            <div class="container">
             <div class="row">
                 <div class="col-md-12 main-title">
                 <h1>اختبار Strong لتحديد الميول</h1>
                <p>نتائج الاختبار</p>
                </div>
       		</div>
            </div>
 	<center>
 
 <div class="row clearfix">
  <div class="col span_4 fwImage">
    <div class="content-slider-bg">
<div id="content-slider-1" class="royalSlider contentSlider rsDefault">
  <div>
    <h3>أنت شخص 
	<?php 
		echo get_theme($theme[0])." و ".get_theme($theme[1]);
	?>
	</h3>
		<div
		 <div id="radar" style="float:left; margin-left:40px;">
					<div style="width:450px">
			<canvas id="Radar" height="450" width="450"></canvas>
		</div>


	<script>
	var radarChartData = {
		labels: ["اجتماعي", "مبدع ", "مغامر", "بحثي", "تقليدي", "واقعي"],
		datasets: [
			{
				label: "My First dataset",
				fillColor: "rgba(0,0,250,0.2)",
				strokeColor: "rgba(220,220,220,1)",
				pointColor: "rgba(220,220,220,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: [
				<?php 
				echo $GOT["social"].",".$GOT["artisic"].",".$GOT["advanture"].",".$GOT["research"].",".$GOT["tradtional"].",".$GOT["realitiy"];
				?>
				]
			}
		]
	};


	</script>
	</div>
    <span class="rsTmb">النتيحة العامة</span>
  </div>
  <div>
    <h3>النتيجة التفصيلية</h3>
	<?php
		print_table($theme);
	?>
	<span class="rsTmb">النتيجة التفصيلية</span>
  </div>
  <div>
</div>
    </div>
  </div>
	


   
  
 <script id="addJS">jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: false,
    fadeinLoadedSlide: false,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    imageScaleMode: 'none',
    imageAlignCenter:false,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false
  });
});
<?php
	print_script();
?>
</script>
  </center>
<footer>
        <section id="footer_2">
    <div class="container">
    <div class="row">
    <div id="copy_rights">
		<div class="col-md-6">
                <ul id="footer-nav">
                    <li>جميع الحقوق محفوظة ©</li>
					<li><a href="#">جامعة دمشق</a></li>
					<li><a href="#">مركز التوجيه المهني</a></li>
                    
                </ul>              
        </div>
	</div>
	<div id="social_container">
            <div class="col-md-6" style="text-align:center">
                <ul class="social-bookmarks clearfix">
                    <li class="delicious"><a href="#">delicious</a></li>
					<li class="googleplus"><a href="#">googleplus</a></li>
					<li class="twitter"><a href="#">twitter</a></li>
					<li class="facebook"><a href="#">facebook</a></li>
                </ul>
            </div>
		</div>
        </div>
		</div>
	</section>
</footer> 
 
 <div id="toTop">العودة إلى الأعلى</div>  
</body>
</html>