<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="ar">
<?php
	include("func.php");
	print_head();
?>

<body>
<?php print_header(); ?>


            <div class="container">
             <div class="row">
                 <div class="col-md-12 main-title">
                 <h1>اختبار Strong لتحديد الميول</h1>
                <p>المرحلة الأولى</p>
                </div>
       		</div>
            </div>

<section class="container" id="main">

<div id="survey_container">

	
		
    
	<form name="example-1" id="wrapped" action="s2.php" method="POST" />
		<?php 
			echo '<input type="hidden" value='.$_POST["user_name"].' name="user_name" />
			';
			echo '<input type="hidden" value='.$_POST["user_age"].' name="user_age" />
			';
			echo '<input type="hidden" value='.$_POST["user_mail"].' name="user_mail" />
			';
			echo '<input type="hidden" value='.$_POST["user_job"].' name="user_job" />
			'; 
			
		?>
		<div id="middle-wizard">
		<center>	 
			<div class="step row">
			<h3>هناكَ بعضُ المهنِ مُدرجةٌ هنا. أَظهِر كيف تشعر حول العملِ في كلِّ مهنةٍ من هذه المهن.</h3>
			
			
				<h3>إذا كنت حقا تعتقد حقا أنك سوف تحب هذا العمل  قم باختيار <u><i>أحب ذلك</i></u></h3>
				<h3>إذا كنت تعتقد أنك سوف تكون غير مبالي بهذا العمل احتر <u><i>لا أهتم بها</i></u></h3>
				<h3>إذا كنت تعتقد أنك لن تحب هذا النوع من العمل اختر <u><i> لا احب ذلك </i></u></h3>
			
	<h3>لا تكنْ قلقاً حولَ ما إذا ستكون جيّداً في العمل أو أنَّك لنْ تكونَ متدرِّباً على هذا العمل. لا تفكِّر بمقدارِ المال الذي يمكن أنْ تجمعَهُ, فكِّر فقط حولَ ما إذا كُنْتَ ستُحبَ أنْ تعملَ في ذلك العمل. </h3>
				
				<hr/>
				
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـ﻿محاسب 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_1" id="s1_1_1" class="css-checkbox" value="1" /><label for="s1_1_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_1" id="s1_1_2" class="css-checkbox" value="2"  /><label for="s1_1_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_1" id="s1_1_3" class="css-checkbox" value="3"  /><label for="s1_1_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـممثّل 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_2" id="s1_2_1" class="css-checkbox" value="1" /><label for="s1_2_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_2" id="s1_2_2" class="css-checkbox" value="2"  /><label for="s1_2_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_2" id="s1_2_3" class="css-checkbox" value="3"  /><label for="s1_2_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـعامل في مجال الإعلانات/ الدعاية
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_3" id="s1_3_1" class="css-checkbox" value="1" /><label for="s1_3_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_3" id="s1_3_2" class="css-checkbox" value="2"  /><label for="s1_3_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_3" id="s1_3_3" class="css-checkbox" value="3"  /><label for="s1_3_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمهندس معماري 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_4" id="s1_4_1" class="css-checkbox" value="1" /><label for="s1_4_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_4" id="s1_4_2" class="css-checkbox" value="2"  /><label for="s1_4_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_4" id="s1_4_3" class="css-checkbox" value="3"  /><label for="s1_4_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمدير متحف فنّي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_5" id="s1_5_1" class="css-checkbox" value="1" /><label for="s1_5_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_5" id="s1_5_2" class="css-checkbox" value="2"  /><label for="s1_5_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_5" id="s1_5_3" class="css-checkbox" value="3"  /><label for="s1_5_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمدرِّس فنون 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_6" id="s1_6_1" class="css-checkbox" value="1" /><label for="s1_6_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_6" id="s1_6_2" class="css-checkbox" value="2"  /><label for="s1_6_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_6" id="s1_6_3" class="css-checkbox" value="3"  /><label for="s1_6_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـفنَّان 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_7" id="s1_7_1" class="css-checkbox" value="1" /><label for="s1_7_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_7" id="s1_7_2" class="css-checkbox" value="2"  /><label for="s1_7_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_7" id="s1_7_3" class="css-checkbox" value="3"  /><label for="s1_7_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـعارض فنّي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_8" id="s1_8_1" class="css-checkbox" value="1" /><label for="s1_8_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_8" id="s1_8_2" class="css-checkbox" value="2"  /><label for="s1_8_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_8" id="s1_8_3" class="css-checkbox" value="3"  /><label for="s1_8_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـفلكي ( يدرس النجوم والكواكب)
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_9" id="s1_9_1" class="css-checkbox" value="1" /><label for="s1_9_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_9" id="s1_9_2" class="css-checkbox" value="2"  /><label for="s1_9_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_9" id="s1_9_3" class="css-checkbox" value="3"  /><label for="s1_9_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمشرف رياضي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_10" id="s1_10_1" class="css-checkbox" value="1" /><label for="s1_10_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_10" id="s1_10_2" class="css-checkbox" value="2"  /><label for="s1_10_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_10" id="s1_10_3" class="css-checkbox" value="3"  /><label for="s1_10_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمؤلِّف كتب للأطفال 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_11" id="s1_11_1" class="css-checkbox" value="1" /><label for="s1_11_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_11" id="s1_11_2" class="css-checkbox" value="2"  /><label for="s1_11_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_11" id="s1_11_3" class="css-checkbox" value="3"  /><label for="s1_11_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمؤلِّف روايات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_12" id="s1_12_1" class="css-checkbox" value="1" /><label for="s1_12_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_12" id="s1_12_2" class="css-checkbox" value="2"  /><label for="s1_12_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_12" id="s1_12_3" class="css-checkbox" value="3"  /><label for="s1_12_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمؤلِّف كتب تقنّية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_13" id="s1_13_1" class="css-checkbox" value="1" /><label for="s1_13_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_13" id="s1_13_2" class="css-checkbox" value="2"  /><label for="s1_13_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_13" id="s1_13_3" class="css-checkbox" value="3"  /><label for="s1_13_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـميكانيكي سيارات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_14" id="s1_14_1" class="css-checkbox" value="1" /><label for="s1_14_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_14" id="s1_14_2" class="css-checkbox" value="2"  /><label for="s1_14_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_14" id="s1_14_3" class="css-checkbox" value="3"  /><label for="s1_14_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمتسابق سيارات ( سائق سيارة في سباق)
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_15" id="s1_15_1" class="css-checkbox" value="1" /><label for="s1_15_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_15" id="s1_15_2" class="css-checkbox" value="2"  /><label for="s1_15_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_15" id="s1_15_3" class="css-checkbox" value="3"  /><label for="s1_15_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـبائع سيارات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_16" id="s1_16_1" class="css-checkbox" value="1" /><label for="s1_16_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_16" id="s1_16_2" class="css-checkbox" value="2"  /><label for="s1_16_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_16" id="s1_16_3" class="css-checkbox" value="3"  /><label for="s1_16_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـصرَّاف بالبنك 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_17" id="s1_17_1" class="css-checkbox" value="1" /><label for="s1_17_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_17" id="s1_17_2" class="css-checkbox" value="2"  /><label for="s1_17_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_17" id="s1_17_3" class="css-checkbox" value="3"  /><label for="s1_17_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمستشار الجمال والعناية بالشعر 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_18" id="s1_18_1" class="css-checkbox" value="1" /><label for="s1_18_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_18" id="s1_18_2" class="css-checkbox" value="2"  /><label for="s1_18_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_18" id="s1_18_3" class="css-checkbox" value="3"  /><label for="s1_18_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمدرِّس للغتين 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_19" id="s1_19_1" class="css-checkbox" value="1" /><label for="s1_19_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_19" id="s1_19_2" class="css-checkbox" value="2"  /><label for="s1_19_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_19" id="s1_19_3" class="css-checkbox" value="3"  /><label for="s1_19_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـعالِم أحياء 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_20" id="s1_20_1" class="css-checkbox" value="1" /><label for="s1_20_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_20" id="s1_20_2" class="css-checkbox" value="2"  /><label for="s1_20_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_20" id="s1_20_3" class="css-checkbox" value="3"  /><label for="s1_20_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمسؤول حسابات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_21" id="s1_21_1" class="css-checkbox" value="1" /><label for="s1_21_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_21" id="s1_21_2" class="css-checkbox" value="2"  /><label for="s1_21_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_21" id="s1_21_3" class="css-checkbox" value="3"  /><label for="s1_21_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمُتعهّد بناء 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_22" id="s1_22_1" class="css-checkbox" value="1" /><label for="s1_22_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_22" id="s1_22_2" class="css-checkbox" value="2"  /><label for="s1_22_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_22" id="s1_22_3" class="css-checkbox" value="3"  /><label for="s1_22_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمدرّس أعمال- مدرّس في مدرسة تجارية
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_23" id="s1_23_1" class="css-checkbox" value="1" /><label for="s1_23_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_23" id="s1_23_2" class="css-checkbox" value="2"  /><label for="s1_23_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_23" id="s1_23_3" class="css-checkbox" value="3"  /><label for="s1_23_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـتاجر 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_24" id="s1_24_1" class="css-checkbox" value="1" /><label for="s1_24_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_24" id="s1_24_2" class="css-checkbox" value="2"  /><label for="s1_24_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_24" id="s1_24_3" class="css-checkbox" value="3"  /><label for="s1_24_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـنجَّار 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_25" id="s1_25_1" class="css-checkbox" value="1" /><label for="s1_25_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_25" id="s1_25_2" class="css-checkbox" value="2"  /><label for="s1_25_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_25" id="s1_25_3" class="css-checkbox" value="3"  /><label for="s1_25_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـرسام كرتون 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_26" id="s1_26_1" class="css-checkbox" value="1" /><label for="s1_26_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_26" id="s1_26_2" class="css-checkbox" value="2"  /><label for="s1_26_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_26" id="s1_26_3" class="css-checkbox" value="3"  /><label for="s1_26_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـأمين صندوق في بنك ( مصرف) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_27" id="s1_27_1" class="css-checkbox" value="1" /><label for="s1_27_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_27" id="s1_27_2" class="css-checkbox" value="2"  /><label for="s1_27_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_27" id="s1_27_3" class="css-checkbox" value="3"  /><label for="s1_27_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـعالم كيمياء 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_28" id="s1_28_1" class="css-checkbox" value="1" /><label for="s1_28_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_28" id="s1_28_2" class="css-checkbox" value="2"  /><label for="s1_28_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_28" id="s1_28_3" class="css-checkbox" value="3"  /><label for="s1_28_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمخطط مدن 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_29" id="s1_29_1" class="css-checkbox" value="1" /><label for="s1_29_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_29" id="s1_29_2" class="css-checkbox" value="2"  /><label for="s1_29_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_29" id="s1_29_3" class="css-checkbox" value="3"  /><label for="s1_29_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمهندس مدني 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_30" id="s1_30_1" class="css-checkbox" value="1" /><label for="s1_30_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_30" id="s1_30_2" class="css-checkbox" value="2"  /><label for="s1_30_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_30" id="s1_30_3" class="css-checkbox" value="3"  /><label for="s1_30_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـموظَّف مصلحة مدنية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_31" id="s1_31_1" class="css-checkbox" value="1" /><label for="s1_31_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_31" id="s1_31_2" class="css-checkbox" value="2"  /><label for="s1_31_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_31" id="s1_31_3" class="css-checkbox" value="3"  /><label for="s1_31_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمصمِّم ملابس 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_32" id="s1_32_1" class="css-checkbox" value="1" /><label for="s1_32_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_32" id="s1_32_2" class="css-checkbox" value="2"  /><label for="s1_32_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_32" id="s1_32_3" class="css-checkbox" value="3"  /><label for="s1_32_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـأستاذ جامعي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_33" id="s1_33_1" class="css-checkbox" value="1" /><label for="s1_33_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_33" id="s1_33_2" class="css-checkbox" value="2"  /><label for="s1_33_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_33" id="s1_33_3" class="css-checkbox" value="3"  /><label for="s1_33_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـصيانة أجهزة الكمبيوتر
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_34" id="s1_34_1" class="css-checkbox" value="1" /><label for="s1_34_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_34" id="s1_34_2" class="css-checkbox" value="2"  /><label for="s1_34_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_34" id="s1_34_3" class="css-checkbox" value="3"  /><label for="s1_34_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُبرمج كمبيوتر 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_35" id="s1_35_1" class="css-checkbox" value="1" /><label for="s1_35_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_35" id="s1_35_2" class="css-checkbox" value="2"  /><label for="s1_35_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_35" id="s1_35_3" class="css-checkbox" value="3"  /><label for="s1_35_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمحامي يختص بقضايا المؤسسات التجارية
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_36" id="s1_36_1" class="css-checkbox" value="1" /><label for="s1_36_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_36" id="s1_36_2" class="css-checkbox" value="2"  /><label for="s1_36_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_36" id="s1_36_3" class="css-checkbox" value="3"  /><label for="s1_36_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـكاتب جلسات المحاكمات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_37" id="s1_37_1" class="css-checkbox" value="1" /><label for="s1_37_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_37" id="s1_37_2" class="css-checkbox" value="2"  /><label for="s1_37_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_37" id="s1_37_3" class="css-checkbox" value="3"  /><label for="s1_37_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمحامي جرمي (جنائي) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_38" id="s1_38_1" class="css-checkbox" value="1" /><label for="s1_38_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_38" id="s1_38_2" class="css-checkbox" value="2"  /><label for="s1_38_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_38" id="s1_38_3" class="css-checkbox" value="3"  /><label for="s1_38_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمندوب خدمات الزبائن
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_39" id="s1_39_1" class="css-checkbox" value="1" /><label for="s1_39_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_39" id="s1_39_2" class="css-checkbox" value="2"  /><label for="s1_39_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_39" id="s1_39_3" class="css-checkbox" value="3"  /><label for="s1_39_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُدرِّس رقص 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_40" id="s1_40_1" class="css-checkbox" value="1" /><label for="s1_40_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_40" id="s1_40_2" class="css-checkbox" value="2"  /><label for="s1_40_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_40" id="s1_40_3" class="css-checkbox" value="3"  /><label for="s1_40_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـعامل في روضة أطفال 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_41" id="s1_41_1" class="css-checkbox" value="1" /><label for="s1_41_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_41" id="s1_41_2" class="css-checkbox" value="2"  /><label for="s1_41_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_41" id="s1_41_3" class="css-checkbox" value="3"  /><label for="s1_41_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمساعد طبيب أسنان 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_42" id="s1_42_1" class="css-checkbox" value="1" /><label for="s1_42_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_42" id="s1_42_2" class="css-checkbox" value="2"  /><label for="s1_42_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_42" id="s1_42_3" class="css-checkbox" value="3"  /><label for="s1_42_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـطبيب أسنان 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_43" id="s1_43_1" class="css-checkbox" value="1" /><label for="s1_43_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_43" id="s1_43_2" class="css-checkbox" value="2"  /><label for="s1_43_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_43" id="s1_43_3" class="css-checkbox" value="3"  /><label for="s1_43_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمصمم أجهزة إلكترونية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_44" id="s1_44_1" class="css-checkbox" value="1" /><label for="s1_44_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_44" id="s1_44_2" class="css-checkbox" value="2"  /><label for="s1_44_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_44" id="s1_44_3" class="css-checkbox" value="3"  /><label for="s1_44_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـعالم بأصول التغذية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_45" id="s1_45_1" class="css-checkbox" value="1" /><label for="s1_45_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_45" id="s1_45_2" class="css-checkbox" value="2"  /><label for="s1_45_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_45" id="s1_45_3" class="css-checkbox" value="3"  /><label for="s1_45_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمحرّر ( أو) رئيس تحرير 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_46" id="s1_46_1" class="css-checkbox" value="1" /><label for="s1_46_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_46" id="s1_46_2" class="css-checkbox" value="2"  /><label for="s1_46_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_46" id="s1_46_3" class="css-checkbox" value="3"  /><label for="s1_46_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمهندس كهربائي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_47" id="s1_47_1" class="css-checkbox" value="1" /><label for="s1_47_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_47" id="s1_47_2" class="css-checkbox" value="2"  /><label for="s1_47_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_47" id="s1_47_3" class="css-checkbox" value="3"  /><label for="s1_47_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـ( فنِّي) اختصاصي إلكترونيات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_48" id="s1_48_1" class="css-checkbox" value="1" /><label for="s1_48_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_48" id="s1_48_2" class="css-checkbox" value="2"  /><label for="s1_48_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_48" id="s1_48_3" class="css-checkbox" value="3"  /><label for="s1_48_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمعلِّم في مدرسة ابتدائية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_49" id="s1_49_1" class="css-checkbox" value="1" /><label for="s1_49_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_49" id="s1_49_2" class="css-checkbox" value="2"  /><label for="s1_49_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_49" id="s1_49_3" class="css-checkbox" value="3"  /><label for="s1_49_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمدير شؤون العمال 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_50" id="s1_50_1" class="css-checkbox" value="1" /><label for="s1_50_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_50" id="s1_50_2" class="css-checkbox" value="2"  /><label for="s1_50_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_50" id="s1_50_3" class="css-checkbox" value="3"  /><label for="s1_50_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمدير مصنع 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_51" id="s1_51_1" class="css-checkbox" value="1" /><label for="s1_51_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_51" id="s1_51_2" class="css-checkbox" value="2"  /><label for="s1_51_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_51" id="s1_51_3" class="css-checkbox" value="3"  /><label for="s1_51_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمُزارع 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_52" id="s1_52_1" class="css-checkbox" value="1" /><label for="s1_52_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_52" id="s1_52_2" class="css-checkbox" value="2"  /><label for="s1_52_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_52" id="s1_52_3" class="css-checkbox" value="3"  /><label for="s1_52_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمُصمِّم أزياء 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_53" id="s1_53_1" class="css-checkbox" value="1" /><label for="s1_53_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_53" id="s1_53_2" class="css-checkbox" value="2"  /><label for="s1_53_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_53" id="s1_53_3" class="css-checkbox" value="3"  /><label for="s1_53_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُحلِّل مالي
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_54" id="s1_54_1" class="css-checkbox" value="1" /><label for="s1_54_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_54" id="s1_54_2" class="css-checkbox" value="2"  /><label for="s1_54_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_54" id="s1_54_3" class="css-checkbox" value="3"  /><label for="s1_54_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمُضيف طيران
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_55" id="s1_55_1" class="css-checkbox" value="1" /><label for="s1_55_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_55" id="s1_55_2" class="css-checkbox" value="2"  /><label for="s1_55_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_55" id="s1_55_3" class="css-checkbox" value="3"  /><label for="s1_55_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـبائع زهور 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_56" id="s1_56_1" class="css-checkbox" value="1" /><label for="s1_56_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_56" id="s1_56_2" class="css-checkbox" value="2"  /><label for="s1_56_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_56" id="s1_56_3" class="css-checkbox" value="3"  /><label for="s1_56_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمُراسل صحيفة أو جريدة في بلد أجنبي
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_57" id="s1_57_1" class="css-checkbox" value="1" /><label for="s1_57_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_57" id="s1_57_2" class="css-checkbox" value="2"  /><label for="s1_57_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_57" id="s1_57_3" class="css-checkbox" value="3"  /><label for="s1_57_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُوظف الخدمات الأجنبية (الخارجية) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_58" id="s1_58_1" class="css-checkbox" value="1" /><label for="s1_58_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_58" id="s1_58_2" class="css-checkbox" value="2"  /><label for="s1_58_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_58" id="s1_58_3" class="css-checkbox" value="3"  /><label for="s1_58_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـحارس في غابة أو حديقة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_59" id="s1_59_1" class="css-checkbox" value="1" /><label for="s1_59_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_59" id="s1_59_2" class="css-checkbox" value="2"  /><label for="s1_59_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_59" id="s1_59_3" class="css-checkbox" value="3"  /><label for="s1_59_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـكاتب مستقل
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_60" id="s1_60_1" class="css-checkbox" value="1" /><label for="s1_60_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_60" id="s1_60_2" class="css-checkbox" value="2"  /><label for="s1_60_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_60" id="s1_60_3" class="css-checkbox" value="3"  /><label for="s1_60_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُحافظ 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_61" id="s1_61_1" class="css-checkbox" value="1" /><label for="s1_61_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_61" id="s1_61_2" class="css-checkbox" value="2"  /><label for="s1_61_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_61" id="s1_61_3" class="css-checkbox" value="3"  /><label for="s1_61_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُدرِّس في المرحلة الثانوية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_62" id="s1_62_1" class="css-checkbox" value="1" /><label for="s1_62_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_62" id="s1_62_2" class="css-checkbox" value="2"  /><label for="s1_62_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_62" id="s1_62_3" class="css-checkbox" value="3"  /><label for="s1_62_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمُوظَّف الاستقبال في مستشفى 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_63" id="s1_63_1" class="css-checkbox" value="1" /><label for="s1_63_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_63" id="s1_63_2" class="css-checkbox" value="2"  /><label for="s1_63_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_63" id="s1_63_3" class="css-checkbox" value="3"  /><label for="s1_63_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُدبّر منزل
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_64" id="s1_64_1" class="css-checkbox" value="1" /><label for="s1_64_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_64" id="s1_64_2" class="css-checkbox" value="2"  /><label for="s1_64_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_64" id="s1_64_3" class="css-checkbox" value="3"  /><label for="s1_64_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُدير فندق 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_65" id="s1_65_1" class="css-checkbox" value="1" /><label for="s1_65_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_65" id="s1_65_2" class="css-checkbox" value="2"  /><label for="s1_65_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_65" id="s1_65_3" class="css-checkbox" value="3"  /><label for="s1_65_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـرسّام 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_66" id="s1_66_1" class="css-checkbox" value="1" /><label for="s1_66_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_66" id="s1_66_2" class="css-checkbox" value="2"  /><label for="s1_66_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_66" id="s1_66_3" class="css-checkbox" value="3"  /><label for="s1_66_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُحاسب ضرائب الدخل 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_67" id="s1_67_1" class="css-checkbox" value="1" /><label for="s1_67_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_67" id="s1_67_2" class="css-checkbox" value="2"  /><label for="s1_67_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_67" id="s1_67_3" class="css-checkbox" value="3"  /><label for="s1_67_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُصمّم ديكورات داخلية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_68" id="s1_68_1" class="css-checkbox" value="1" /><label for="s1_68_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_68" id="s1_68_2" class="css-checkbox" value="2"  /><label for="s1_68_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_68" id="s1_68_3" class="css-checkbox" value="3"  /><label for="s1_68_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـكابتن طيارة نفاثة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_69" id="s1_69_1" class="css-checkbox" value="1" /><label for="s1_69_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_69" id="s1_69_2" class="css-checkbox" value="2"  /><label for="s1_69_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_69" id="s1_69_3" class="css-checkbox" value="3"  /><label for="s1_69_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـقاضي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_70" id="s1_70_1" class="css-checkbox" value="1" /><label for="s1_70_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_70" id="s1_70_2" class="css-checkbox" value="2"  /><label for="s1_70_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_70" id="s1_70_3" class="css-checkbox" value="3"  /><label for="s1_70_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمُراقب للعمال 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_71" id="s1_71_1" class="css-checkbox" value="1" /><label for="s1_71_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_71" id="s1_71_2" class="css-checkbox" value="2"  /><label for="s1_71_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_71" id="s1_71_3" class="css-checkbox" value="3"  /><label for="s1_71_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـتقني في مختبر
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_72" id="s1_72_1" class="css-checkbox" value="1" /><label for="s1_72_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_72" id="s1_72_2" class="css-checkbox" value="2"  /><label for="s1_72_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_72" id="s1_72_3" class="css-checkbox" value="3"  /><label for="s1_72_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـجنيناتي ( مختص بتنظيم الحدائق)
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_73" id="s1_73_1" class="css-checkbox" value="1" /><label for="s1_73_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_73" id="s1_73_2" class="css-checkbox" value="2"  /><label for="s1_73_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_73" id="s1_73_3" class="css-checkbox" value="3"  /><label for="s1_73_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـأمين مكتبة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_74" id="s1_74_1" class="css-checkbox" value="1" /><label for="s1_74_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_74" id="s1_74_2" class="css-checkbox" value="2"  /><label for="s1_74_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_74" id="s1_74_3" class="css-checkbox" value="3"  /><label for="s1_74_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمندوب شركة تأمين 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_75" id="s1_75_1" class="css-checkbox" value="1" /><label for="s1_75_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_75" id="s1_75_2" class="css-checkbox" value="2"  /><label for="s1_75_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_75" id="s1_75_3" class="css-checkbox" value="3"  /><label for="s1_75_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمشرف على ورشة آلات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_76" id="s1_76_1" class="css-checkbox" value="1" /><label for="s1_76_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_76" id="s1_76_2" class="css-checkbox" value="2"  /><label for="s1_76_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_76" id="s1_76_3" class="css-checkbox" value="3"  /><label for="s1_76_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـميكانيكي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_77" id="s1_77_1" class="css-checkbox" value="1" /><label for="s1_77_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_77" id="s1_77_2" class="css-checkbox" value="2"  /><label for="s1_77_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_77" id="s1_77_3" class="css-checkbox" value="3"  /><label for="s1_77_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمدير غرفة التجارة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_78" id="s1_78_1" class="css-checkbox" value="1" /><label for="s1_78_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_78" id="s1_78_2" class="css-checkbox" value="2"  /><label for="s1_78_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_78" id="s1_78_3" class="css-checkbox" value="3"  /><label for="s1_78_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُدير مركز عناية بالأطفال 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_79" id="s1_79_1" class="css-checkbox" value="1" /><label for="s1_79_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_79" id="s1_79_2" class="css-checkbox" value="2"  /><label for="s1_79_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_79" id="s1_79_3" class="css-checkbox" value="3"  /><label for="s1_79_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُدير مخزن ألبسة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_80" id="s1_80_1" class="css-checkbox" value="1" /><label for="s1_80_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_80" id="s1_80_2" class="css-checkbox" value="2"  /><label for="s1_80_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_80" id="s1_80_3" class="css-checkbox" value="3"  /><label for="s1_80_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـعامل إنتاج في مصنع 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_81" id="s1_81_1" class="css-checkbox" value="1" /><label for="s1_81_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_81" id="s1_81_2" class="css-checkbox" value="2"  /><label for="s1_81_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_81" id="s1_81_3" class="css-checkbox" value="3"  /><label for="s1_81_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمهندس ميكانيك
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_82" id="s1_82_1" class="css-checkbox" value="1" /><label for="s1_82_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_82" id="s1_82_2" class="css-checkbox" value="2"  /><label for="s1_82_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_82" id="s1_82_3" class="css-checkbox" value="3"  /><label for="s1_82_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـضابط في الجيش 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_83" id="s1_83_1" class="css-checkbox" value="1" /><label for="s1_83_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_83" id="s1_83_2" class="css-checkbox" value="2"  /><label for="s1_83_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_83" id="s1_83_3" class="css-checkbox" value="3"  /><label for="s1_83_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـموسيقيّ 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_84" id="s1_84_1" class="css-checkbox" value="1" /><label for="s1_84_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_84" id="s1_84_2" class="css-checkbox" value="2"  /><label for="s1_84_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_84" id="s1_84_3" class="css-checkbox" value="3"  /><label for="s1_84_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمُراسل صحفي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_85" id="s1_85_1" class="css-checkbox" value="1" /><label for="s1_85_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_85" id="s1_85_2" class="css-checkbox" value="2"  /><label for="s1_85_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_85" id="s1_85_3" class="css-checkbox" value="3"  /><label for="s1_85_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـممرضة/ ممرض 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_86" id="s1_86_1" class="css-checkbox" value="1" /><label for="s1_86_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_86" id="s1_86_2" class="css-checkbox" value="2"  /><label for="s1_86_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_86" id="s1_86_3" class="css-checkbox" value="3"  /><label for="s1_86_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمساعد ممرض/ مساعدة ممرضة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_87" id="s1_87_1" class="css-checkbox" value="1" /><label for="s1_87_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_87" id="s1_87_2" class="css-checkbox" value="2"  /><label for="s1_87_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_87" id="s1_87_3" class="css-checkbox" value="3"  /><label for="s1_87_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـموظَّف
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_88" id="s1_88_1" class="css-checkbox" value="1" /><label for="s1_88_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_88" id="s1_88_2" class="css-checkbox" value="2"  /><label for="s1_88_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_88" id="s1_88_3" class="css-checkbox" value="3"  /><label for="s1_88_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمدير مكتب
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_89" id="s1_89_1" class="css-checkbox" value="1" /><label for="s1_89_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_89" id="s1_89_2" class="css-checkbox" value="2"  /><label for="s1_89_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_89" id="s1_89_3" class="css-checkbox" value="3"  /><label for="s1_89_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمغني أوبرا 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_90" id="s1_90_1" class="css-checkbox" value="1" /><label for="s1_90_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_90" id="s1_90_2" class="css-checkbox" value="2"  /><label for="s1_90_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_90" id="s1_90_3" class="css-checkbox" value="3"  /><label for="s1_90_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـقائد فرقة موسيقية
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_91" id="s1_91_1" class="css-checkbox" value="1" /><label for="s1_91_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_91" id="s1_91_2" class="css-checkbox" value="2"  /><label for="s1_91_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_91" id="s1_91_3" class="css-checkbox" value="3"  /><label for="s1_91_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمساعد محامي
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_92" id="s1_92_1" class="css-checkbox" value="1" /><label for="s1_92_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_92" id="s1_92_2" class="css-checkbox" value="2"  /><label for="s1_92_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_92" id="s1_92_3" class="css-checkbox" value="3"  /><label for="s1_92_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمساعد طبيب / أو طبيب مساعد 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_93" id="s1_93_1" class="css-checkbox" value="1" /><label for="s1_93_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_93" id="s1_93_2" class="css-checkbox" value="2"  /><label for="s1_93_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_93" id="s1_93_3" class="css-checkbox" value="3"  /><label for="s1_93_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـصيدلاني
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_94" id="s1_94_1" class="css-checkbox" value="1" /><label for="s1_94_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_94" id="s1_94_2" class="css-checkbox" value="2"  /><label for="s1_94_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_94" id="s1_94_3" class="css-checkbox" value="3"  /><label for="s1_94_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمصوِّر 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_95" id="s1_95_1" class="css-checkbox" value="1" /><label for="s1_95_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_95" id="s1_95_2" class="css-checkbox" value="2"  /><label for="s1_95_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_95" id="s1_95_3" class="css-checkbox" value="3"  /><label for="s1_95_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـطبيب ( استشاري, غير جرّاح) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_96" id="s1_96_1" class="css-checkbox" value="1" /><label for="s1_96_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_96" id="s1_96_2" class="css-checkbox" value="2"  /><label for="s1_96_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_96" id="s1_96_3" class="css-checkbox" value="3"  /><label for="s1_96_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمدير ملعب 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_97" id="s1_97_1" class="css-checkbox" value="1" /><label for="s1_97_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_97" id="s1_97_2" class="css-checkbox" value="2"  /><label for="s1_97_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_97" id="s1_97_3" class="css-checkbox" value="3"  /><label for="s1_97_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـشاعر 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_98" id="s1_98_1" class="css-checkbox" value="1" /><label for="s1_98_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_98" id="s1_98_2" class="css-checkbox" value="2"  /><label for="s1_98_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_98" id="s1_98_3" class="css-checkbox" value="3"  /><label for="s1_98_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـضابط شرطة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_99" id="s1_99_1" class="css-checkbox" value="1" /><label for="s1_99_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_99" id="s1_99_2" class="css-checkbox" value="2"  /><label for="s1_99_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_99" id="s1_99_3" class="css-checkbox" value="3"  /><label for="s1_99_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـسياسي
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_100" id="s1_100_1" class="css-checkbox" value="1" /><label for="s1_100_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_100" id="s1_100_2" class="css-checkbox" value="2"  /><label for="s1_100_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_100" id="s1_100_3" class="css-checkbox" value="3"  /><label for="s1_100_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـسكرتير خاص 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_101" id="s1_101_1" class="css-checkbox" value="1" /><label for="s1_101_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_101" id="s1_101_2" class="css-checkbox" value="2"  /><label for="s1_101_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_101" id="s1_101_3" class="css-checkbox" value="3"  /><label for="s1_101_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـرياضي محترف 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_102" id="s1_102_1" class="css-checkbox" value="1" /><label for="s1_102_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_102" id="s1_102_2" class="css-checkbox" value="2"  /><label for="s1_102_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_102" id="s1_102_3" class="css-checkbox" value="3"  /><label for="s1_102_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـراقص محترف 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_103" id="s1_103_1" class="css-checkbox" value="1" /><label for="s1_103_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_103" id="s1_103_2" class="css-checkbox" value="2"  /><label for="s1_103_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_103" id="s1_103_3" class="css-checkbox" value="3"  /><label for="s1_103_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـعالِم نفس 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_104" id="s1_104_1" class="css-checkbox" value="1" /><label for="s1_104_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_104" id="s1_104_2" class="css-checkbox" value="2"  /><label for="s1_104_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_104" id="s1_104_3" class="css-checkbox" value="3"  /><label for="s1_104_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمدير علاقات عامة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_105" id="s1_105_1" class="css-checkbox" value="1" /><label for="s1_105_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_105" id="s1_105_2" class="css-checkbox" value="2"  /><label for="s1_105_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_105" id="s1_105_3" class="css-checkbox" value="3"  /><label for="s1_105_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـصاحب مزرعة تربية خيول وماشية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_106" id="s1_106_1" class="css-checkbox" value="1" /><label for="s1_106_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_106" id="s1_106_2" class="css-checkbox" value="2"  /><label for="s1_106_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_106" id="s1_106_3" class="css-checkbox" value="3"  /><label for="s1_106_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـسمسار عقارات (يتوسط في بيع وشراء الأراضي والمباني) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_107" id="s1_107_1" class="css-checkbox" value="1" /><label for="s1_107_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_107" id="s1_107_2" class="css-checkbox" value="2"  /><label for="s1_107_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_107" id="s1_107_3" class="css-checkbox" value="3"  /><label for="s1_107_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـموظف استقبال 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_108" id="s1_108_1" class="css-checkbox" value="1" /><label for="s1_108_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_108" id="s1_108_2" class="css-checkbox" value="2"  /><label for="s1_108_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_108" id="s1_108_3" class="css-checkbox" value="3"  /><label for="s1_108_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـزعيم ديني 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_109" id="s1_109_1" class="css-checkbox" value="1" /><label for="s1_109_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_109" id="s1_109_2" class="css-checkbox" value="2"  /><label for="s1_109_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_109" id="s1_109_3" class="css-checkbox" value="3"  /><label for="s1_109_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـبائع بالتجزئة ( بالمفرَّق) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_110" id="s1_110_1" class="css-checkbox" value="1" /><label for="s1_110_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_110" id="s1_110_2" class="css-checkbox" value="2"  /><label for="s1_110_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_110" id="s1_110_3" class="css-checkbox" value="3"  /><label for="s1_110_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمدير مبيعات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_111" id="s1_111_1" class="css-checkbox" value="1" /><label for="s1_111_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_111" id="s1_111_2" class="css-checkbox" value="2"  /><label for="s1_111_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_111" id="s1_111_3" class="css-checkbox" value="3"  /><label for="s1_111_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمدير مدرسة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_112" id="s1_112_1" class="css-checkbox" value="1" /><label for="s1_112_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_112" id="s1_112_2" class="css-checkbox" value="2"  /><label for="s1_112_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_112" id="s1_112_3" class="css-checkbox" value="3"  /><label for="s1_112_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـرسَّام علمي ( اختصاصي رسم مواد علمية) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_113" id="s1_113_1" class="css-checkbox" value="1" /><label for="s1_113_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_113" id="s1_113_2" class="css-checkbox" value="2"  /><label for="s1_113_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_113" id="s1_113_3" class="css-checkbox" value="3"  /><label for="s1_113_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـعامل في مجال البحث العلمي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_114" id="s1_114_1" class="css-checkbox" value="1" /><label for="s1_114_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_114" id="s1_114_2" class="css-checkbox" value="2"  /><label for="s1_114_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_114" id="s1_114_3" class="css-checkbox" value="3"  /><label for="s1_114_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـنحَّات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_115" id="s1_115_1" class="css-checkbox" value="1" /><label for="s1_115_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_115" id="s1_115_2" class="css-checkbox" value="2"  /><label for="s1_115_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_115" id="s1_115_3" class="css-checkbox" value="3"  /><label for="s1_115_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـعميل سريّ 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_116" id="s1_116_1" class="css-checkbox" value="1" /><label for="s1_116_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_116" id="s1_116_2" class="css-checkbox" value="2"  /><label for="s1_116_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_116" id="s1_116_3" class="css-checkbox" value="3"  /><label for="s1_116_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـحارس أمن 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_117" id="s1_117_1" class="css-checkbox" value="1" /><label for="s1_117_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_117" id="s1_117_2" class="css-checkbox" value="2"  /><label for="s1_117_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_117" id="s1_117_3" class="css-checkbox" value="3"  /><label for="s1_117_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمرشد اجتماعي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_118" id="s1_118_1" class="css-checkbox" value="1" /><label for="s1_118_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_118" id="s1_118_2" class="css-checkbox" value="2"  /><label for="s1_118_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_118" id="s1_118_3" class="css-checkbox" value="3"  /><label for="s1_118_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمعلم تربية خاصة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_119" id="s1_119_1" class="css-checkbox" value="1" /><label for="s1_119_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_119" id="s1_119_2" class="css-checkbox" value="2"  /><label for="s1_119_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_119" id="s1_119_3" class="css-checkbox" value="3"  /><label for="s1_119_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـبائع مختص ببيع سلعة معينة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_120" id="s1_120_1" class="css-checkbox" value="1" /><label for="s1_120_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_120" id="s1_120_2" class="css-checkbox" value="2"  /><label for="s1_120_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_120" id="s1_120_3" class="css-checkbox" value="3"  /><label for="s1_120_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمراسل رياضي (صحفي مختص بالرياضة) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_121" id="s1_121_1" class="css-checkbox" value="1" /><label for="s1_121_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_121" id="s1_121_2" class="css-checkbox" value="2"  /><label for="s1_121_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_121" id="s1_121_3" class="css-checkbox" value="3"  /><label for="s1_121_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـخبير إحصائي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_122" id="s1_122_1" class="css-checkbox" value="1" /><label for="s1_122_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_122" id="s1_122_2" class="css-checkbox" value="2"  /><label for="s1_122_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_122" id="s1_122_3" class="css-checkbox" value="3"  /><label for="s1_122_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـسمسار بورصة ( يعمل في تجارة العملات) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_123" id="s1_123_1" class="css-checkbox" value="1" /><label for="s1_123_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_123" id="s1_123_2" class="css-checkbox" value="2"  /><label for="s1_123_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_123" id="s1_123_3" class="css-checkbox" value="3"  /><label for="s1_123_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـطبيب جرَّاح 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_124" id="s1_124_1" class="css-checkbox" value="1" /><label for="s1_124_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_124" id="s1_124_2" class="css-checkbox" value="2"  /><label for="s1_124_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_124" id="s1_124_3" class="css-checkbox" value="3"  /><label for="s1_124_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـصانع أدوات
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_125" id="s1_125_1" class="css-checkbox" value="1" /><label for="s1_125_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_125" id="s1_125_2" class="css-checkbox" value="2"  /><label for="s1_125_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_125" id="s1_125_3" class="css-checkbox" value="3"  /><label for="s1_125_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تعمل كـمدير وكالة سفريات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_126" id="s1_126_1" class="css-checkbox" value="1" /><label for="s1_126_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_126" id="s1_126_2" class="css-checkbox" value="2"  /><label for="s1_126_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_126" id="s1_126_3" class="css-checkbox" value="3"  /><label for="s1_126_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـبائع جوَّال 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_127" id="s1_127_1" class="css-checkbox" value="1" /><label for="s1_127_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_127" id="s1_127_2" class="css-checkbox" value="2"  /><label for="s1_127_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_127" id="s1_127_3" class="css-checkbox" value="3"  /><label for="s1_127_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمذيع تلفزيوني 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_128" id="s1_128_1" class="css-checkbox" value="1" /><label for="s1_128_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_128" id="s1_128_2" class="css-checkbox" value="2"  /><label for="s1_128_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_128" id="s1_128_3" class="css-checkbox" value="3"  /><label for="s1_128_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـمرشد مهني 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_129" id="s1_129_1" class="css-checkbox" value="1" /><label for="s1_129_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_129" id="s1_129_2" class="css-checkbox" value="2"  /><label for="s1_129_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_129" id="s1_129_3" class="css-checkbox" value="3"  /><label for="s1_129_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـنادل في مطعم
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_130" id="s1_130_1" class="css-checkbox" value="1" /><label for="s1_130_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_130" id="s1_130_2" class="css-checkbox" value="2"  /><label for="s1_130_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_130" id="s1_130_3" class="css-checkbox" value="3"  /><label for="s1_130_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـبائع وموزِّع بضائع بالجملة للتجار 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_131" id="s1_131_1" class="css-checkbox" value="1" /><label for="s1_131_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_131" id="s1_131_2" class="css-checkbox" value="2"  /><label for="s1_131_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_131" id="s1_131_3" class="css-checkbox" value="3"  /><label for="s1_131_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـمعالج نصوص 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_132" id="s1_132_1" class="css-checkbox" value="1" /><label for="s1_132_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_132" id="s1_132_2" class="css-checkbox" value="2"  /><label for="s1_132_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_132" id="s1_132_3" class="css-checkbox" value="3"  /><label for="s1_132_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـفنّي أشعّة إكس ( أشعة تُستخدم في التصوير الشعاعي للعظام)
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_133" id="s1_133_1" class="css-checkbox" value="1" /><label for="s1_133_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_133" id="s1_133_2" class="css-checkbox" value="2"  /><label for="s1_133_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_133" id="s1_133_3" class="css-checkbox" value="3"  /><label for="s1_133_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل العمل كـرجل دِين 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_134" id="s1_134_1" class="css-checkbox" value="1" /><label for="s1_134_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_134" id="s1_134_2" class="css-checkbox" value="2"  /><label for="s1_134_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_134" id="s1_134_3" class="css-checkbox" value="3"  /><label for="s1_134_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في العمل كـعضو في تنظيم شبابي مثل: اتحاد شبيبة الثورة, الكشافة, الخ
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s1_135" id="s1_135_1" class="css-checkbox" value="1" /><label for="s1_135_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s1_135" id="s1_135_2" class="css-checkbox" value="2"  /><label for="s1_135_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s1_135" id="s1_135_3" class="css-checkbox" value="3"  /><label for="s1_135_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			

			<hr/>
			<div class="submit" id="complete">
						<h3>انتهت المرحلة الأولى</h3>
						<br/>
						<h2>لقد قمت بالإجابة عل جميع أسئلة المرحلة الأولى</h2>
						<button type="submit" name="process" class="submit">الانتقال إلى المرحلة الثانية</button>
			</div><!-- end submit step -->
			</div><!-- end step -->
           </center> 
		</div><!-- end middle-wizard -->
	</form>
    
</div><!-- end Survey container -->

</section><!-- end section main container -->
<?php print_footer(); ?>
 <div id="toTop">العودة إلى الأعلى</div>  

</body>
</html>