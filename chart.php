<?php
	include("HeyperX.php");
	include("func.php");
	
?>
 <html>
 <!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="ar">
<!--<![endif]-->
<head>

<!-- Basic Page Needs -->
<meta charset="utf-8" />
<title>اختبار Strong لتحديد الميول</title>
<meta name="description" content="" />
<meta name="author" content="Mohamad Al Jasem" />
<!-- Favicons-->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- CSS -->
<link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="css/socialize-bookmarks.css" rel="stylesheet" />
<!-- Jquery -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.8.12.min.js"></script>
<!-- Chart JS -->
 <script src="js/Chart.js"></script>

<!-- HTML5 and CSS3-in older browsers-->
<script src="js/modernizr.custom.17475.js"></script>

<!-- Support media queries for IE8 -->
<script src="js/respond.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

    

 <body>
 <script src="js/Chart.js"></script>
 <style>
 table
 {
	width:100%;
 }
 #user_profile
 {
	direction:rtl;
 }
 </style>


	
 <?php print_header(); ?>
 <hr/>
 <?php print_profile();  ?>
 <hr/>
   <div style="margin-top:20px;">
    <h3>أنت شخص 
	<?php 
		echo get_theme($theme[0])." و ".get_theme($theme[1]);
	?>
	</h3>
	<hr/>
		<div
		 <div id="radar" style="float:left; margin-left:40px;">
					<div style="width:450px">
			<canvas id="Radar" height="450" width="450"></canvas>
		</div>
<script>
	var radarChartData = {
		labels: ["واقعي","بحثي","فني ","اجتماعي",  "مغامر", "تقليدي"],
		datasets: [
			{
				label: "My First dataset",
				fillColor: "rgba(0,0,250,0.2)",
				strokeColor: "rgba(220,220,220,1)",
				pointColor: "rgba(220,220,220,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: [
				<?php 
				echo $GOT["realitiy"].",".$GOT["research"].",".$GOT["artisic"].",".$GOT["social"].",".$GOT["advanture"].",".$GOT["tradtional"];
				?>
				]
			}
		]
	};


	</script>
	</div>
	<style>
	#detals h1
{
	border-width : 5px;
	text-align: center;
	vertical-align: center;
	padding-bottom:30px;
	padding-right:30px;
	height:200px;
}
.desc
{
	direction:rtl;
	list-style:initial;
	font-size:16pt;
}
.desc h2
{
	padding-bottom:15px;
	padding-top:15px;
	color:white;
}

.upper_table td
{
	font-size:16pt;
	width:33%;
}
.down_table td
{
	font-size:16pt;
	width:50%;
}
.down_table h3
{
	text-decoration: underline;
}
.upper_table h3
{
	text-decoration: underline;
}
#desc_1 h3
{
	color:<?php $x=$theme[0]."_2";echo color($$x); ?>
}
#desc_2 h3
{
	color:<?php $x=$theme[1]."_2";echo color($$x); ?>
}
.top_names ;
{
	width:50%;
	
}
#top_BIS h3
{
	padding-bottom:15px;
	padding-top:15px;
	color:white;
}
#top_BIS p
{
	direction:rtl;
	font-size:14pt;
}
#top_BIS
{
		direction:rtl;
}
	</style>
	<div id="detals" style="float:right;">
	<h1><?php echo get_title($theme[0]); ?></h1>
	<h1><?php echo get_title($theme[1]); ?></h1>
	</div>
</div>

 <?php print_table($theme);  ?>
 <hr/>
 <div id="desc_1">
 <?php print_desc($theme[0]); ?>
 </div>
 <hr/>
 <div id="desc_2">
 <?php print_desc($theme[1]); ?>
 </div>
 <hr/>
 <center>
 <?php print_top_bar(); ?>
 <hr/>
 </center>
 <?php
	top_table();
 ?>
 
 <script>
 <?php
	print_script();
?>
</script>
 <?php print_footer(); ?>
 </body>
 </html>