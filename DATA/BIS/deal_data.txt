		هذا المقياس الجديد ضمن الموضوع التقليدي يقيس عملية التعامل مع البيانات والمعلومات ، ولكن بقدر أكبر من استقلالية الإدارة واتخاذ القرار مقارنة بمقياس الخدمات المكتبية ، الذي كان المقياس الوحيد في الموضوع التقليدي سابقاً ، حيث تحول اسم هذا المقياس من مقياس
( مزاولة الأعمال المكتبية ) في مقياس عام 1985 ، إلى مقياس الخدمات المكتبية في مقياس عام 1994 . إن المحتوى الرئيسي لهذا المقياس هو الولع بالبيانات التي تتعلق بالإحصاء ، التحليل المالي ، على عكس مقياس الخدمات المكتبية ، والأشخاص الذين يحصلون 
على درجات عالية على مقياس التعامل مع البيانات سوف يحصلون على الأرجح أيضاً على درجات عالية في مقياس الرياضيات .
إن مقياس التعامل مع البيانات لم يرتبط فعلياً بأي من مقاييس الأنماط الشخصية الأربعة .
وبالتالي فإن الذين يحصلون على درجات عالية على هذا المقياس لا يفضلون العمل في نمط معين أو مقياس نمط القيادة .
إن الذين يحصلون على درجات مرتفعة في مقياس التعامل مع البيانات هم أشخاص يستمتعون بالتعامل مع البيانات ، ولكنهم قد يستمتعون أو قد لا يستمتعوا بالتعامل مع الناس .
والأشخاص الذين يحصلون على تقدير ( اهتمام عالي ) أو ( اهتمام عالي جداً ) في مقياس التعامل مع البيانات يتضمنون هؤلاء الذين يعملون بالمحاسبة ، مثل ( المحاسبون ، مدراء الائتمان ، المصرفيون ) بالإضافة إلى أولئك الذين يعملون في مجال الرياضيان مثل ( معلمو الرياضيات ، ومحاسبو التأمين ) .
بعض البنود على مقياس التعامل مع البيانات :
1-	محاسب . 2- محلل مالي . 3- عمل مخططات إحصائية . 4- الأشياء مقابل البيانات . 
