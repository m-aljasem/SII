<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="ar">
<?php
	include("func.php");
	print_head();
?>

<body>
<?php print_header(); ?>


            <div class="container">
             <div class="row">
                 <div class="col-md-12 main-title">
                 <h1>اختبار Strong لتحديد الميول</h1>
                <p>المرحلة الخامسة</p>
                </div>
       		</div>
            </div>

<section class="container" id="main">

<div id="survey_container">
   
	<form name="example-1" id="wrapped" action="s6.php" method="POST" />
					<?php	for($i=1;$i<136;$i++)
	{
		echo '<input type="hidden" name="s1_'.$i.'" value="'.$_POST["s1_$i"].'" />
		';
	}
	for($i=1;$i<40;$i++)
	{
		echo '<input type="hidden" name="s2_'.$i.'" value="'.$_POST["s2_$i"].'" />
		';
	}
	for($i=1;$i<47;$i++)
	{
		echo '<input type="hidden" name="s3_'.$i.'" value="'.$_POST["s3_$i"].'" />
		';
	}
	for($i=1;$i<30;$i++)
	{
		echo '<input type="hidden" name="s4_'.$i.'" value="'.$_POST["s4_$i"].'" />
		';
	}
			echo '<input type="hidden" value='.$_POST["user_name"].' name="user_name" />
			';
			echo '<input type="hidden" value='.$_POST["user_age"].' name="user_age" />
			';
			echo '<input type="hidden" value='.$_POST["user_mail"].' name="user_mail" />
			';
			echo '<input type="hidden" value='.$_POST["user_job"].' name="user_job" />
			'; 
	?>
		<div id="middle-wizard">
		<center>
		<div class="step row">
			<h3>إنَّ معظمَنا يختارُ أعمالاً تسمح لنا بالعمل مع النّاس الذين نستمتعُ معهم</h3>
			<h3>أظهِر كيف ستشعُر حولَ اتصالِك اليوميّ مع الأنماط التالية من الناس لا تتذكّر أُناساً معيّنين تعرفهم</h3>
			<h3>إذا كنت تحب التعامل مع مجموعة الاشخاص المذكورة اختر<u><i> أحب ذلك </i></u></h3>
			<h3>إذا كنت لا تهتم بمجموعة الأشخاص المذكورة اختر<u><i> لا أهتم بهم</i></u></h3>
			<h3>إذا كنت لا تحب التعامل مع مجموعة الأشخاص المذكورة اختر <u><i>لا أحبهم</i></u></h3>
			<h3>أعطِ الاستجابة الأولى التي تخطر في ذهنك</h3>
		
		<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل التعامل مع ﻿عمَّال إنشاء الطرق العامة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_1" id="s5_1_1" class="css-checkbox" value="1" /><label for="s5_1_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_1" id="s5_1_2" class="css-checkbox" value="2"  /><label for="s5_1_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_1" id="s5_1_3" class="css-checkbox" value="3"  /><label for="s5_1_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التعامل مع طلاب المدارس الثانوية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_2" id="s5_2_1" class="css-checkbox" value="1" /><label for="s5_2_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_2" id="s5_2_2" class="css-checkbox" value="2"  /><label for="s5_2_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_2" id="s5_2_3" class="css-checkbox" value="3"  /><label for="s5_2_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب التعامل مع الضّباط العسكريون 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_3" id="s5_3_1" class="css-checkbox" value="1" /><label for="s5_3_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_3" id="s5_3_2" class="css-checkbox" value="2"  /><label for="s5_3_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_3" id="s5_3_3" class="css-checkbox" value="3"  /><label for="s5_3_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب التعامل مع الفنانون 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_4" id="s5_4_1" class="css-checkbox" value="1" /><label for="s5_4_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_4" id="s5_4_2" class="css-checkbox" value="2"  /><label for="s5_4_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_4" id="s5_4_3" class="css-checkbox" value="3"  /><label for="s5_4_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التعامل مع راقصوّ الباليه 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_5" id="s5_5_1" class="css-checkbox" value="1" /><label for="s5_5_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_5" id="s5_5_2" class="css-checkbox" value="2"  /><label for="s5_5_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_5" id="s5_5_3" class="css-checkbox" value="3"  /><label for="s5_5_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التعامل مع المتمردّون 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_6" id="s5_6_1" class="css-checkbox" value="1" /><label for="s5_6_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_6" id="s5_6_2" class="css-checkbox" value="2"  /><label for="s5_6_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_6" id="s5_6_3" class="css-checkbox" value="3"  /><label for="s5_6_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التعامل مع القياديّون
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_7" id="s5_7_1" class="css-checkbox" value="1" /><label for="s5_7_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_7" id="s5_7_2" class="css-checkbox" value="2"  /><label for="s5_7_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_7" id="s5_7_3" class="css-checkbox" value="3"  /><label for="s5_7_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التعامل مع المتدّينون 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_8" id="s5_8_1" class="css-checkbox" value="1" /><label for="s5_8_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_8" id="s5_8_2" class="css-checkbox" value="2"  /><label for="s5_8_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_8" id="s5_8_3" class="css-checkbox" value="3"  /><label for="s5_8_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب التعامل مع العدوانّيون 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_9" id="s5_9_1" class="css-checkbox" value="1" /><label for="s5_9_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_9" id="s5_9_2" class="css-checkbox" value="2"  /><label for="s5_9_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_9" id="s5_9_3" class="css-checkbox" value="3"  /><label for="s5_9_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التعامل مع المرضى بدنياً 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_10" id="s5_10_1" class="css-checkbox" value="1" /><label for="s5_10_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_10" id="s5_10_2" class="css-checkbox" value="2"  /><label for="s5_10_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_10" id="s5_10_3" class="css-checkbox" value="3"  /><label for="s5_10_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التعامل مع الأطفال 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_11" id="s5_11_1" class="css-checkbox" value="1" /><label for="s5_11_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_11" id="s5_11_2" class="css-checkbox" value="2"  /><label for="s5_11_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_11" id="s5_11_3" class="css-checkbox" value="3"  /><label for="s5_11_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التعامل مع الكبار جداً في السن 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_12" id="s5_12_1" class="css-checkbox" value="1" /><label for="s5_12_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_12" id="s5_12_2" class="css-checkbox" value="2"  /><label for="s5_12_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_12" id="s5_12_3" class="css-checkbox" value="3"  /><label for="s5_12_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب التعامل مع الذين جمعوا ثروة من العمل 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_13" id="s5_13_1" class="css-checkbox" value="1" /><label for="s5_13_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_13" id="s5_13_2" class="css-checkbox" value="2"  /><label for="s5_13_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_13" id="s5_13_3" class="css-checkbox" value="3"  /><label for="s5_13_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب التعامل مع عباقرة الموسيقى 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_14" id="s5_14_1" class="css-checkbox" value="1" /><label for="s5_14_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_14" id="s5_14_2" class="css-checkbox" value="2"  /><label for="s5_14_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_14" id="s5_14_3" class="css-checkbox" value="3"  /><label for="s5_14_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب التعامل مع المعوّقون بدنيا 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_15" id="s5_15_1" class="css-checkbox" value="1" /><label for="s5_15_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_15" id="s5_15_2" class="css-checkbox" value="2"  /><label for="s5_15_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_15" id="s5_15_3" class="css-checkbox" value="3"  /><label for="s5_15_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب التعامل مع المبتكرون 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_16" id="s5_16_1" class="css-checkbox" value="1" /><label for="s5_16_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_16" id="s5_16_2" class="css-checkbox" value="2"  /><label for="s5_16_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_16" id="s5_16_3" class="css-checkbox" value="3"  /><label for="s5_16_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التعامل مع رجال الأعمال البارزين 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_17" id="s5_17_1" class="css-checkbox" value="1" /><label for="s5_17_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_17" id="s5_17_2" class="css-checkbox" value="2"  /><label for="s5_17_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_17" id="s5_17_3" class="css-checkbox" value="3"  /><label for="s5_17_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل التعامل مع الرياضيّون 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_18" id="s5_18_1" class="css-checkbox" value="1" /><label for="s5_18_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_18" id="s5_18_2" class="css-checkbox" value="2"  /><label for="s5_18_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_18" id="s5_18_3" class="css-checkbox" value="3"  /><label for="s5_18_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب التعامل مع العلماء البارزون 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_19" id="s5_19_1" class="css-checkbox" value="1" /><label for="s5_19_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_19" id="s5_19_2" class="css-checkbox" value="2"  /><label for="s5_19_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_19" id="s5_19_3" class="css-checkbox" value="3"  /><label for="s5_19_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب التعامل مع الذين يعيشون حياة محفوفة بالمخاطر
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s5_20" id="s5_20_1" class="css-checkbox" value="1" /><label for="s5_20_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s5_20" id="s5_20_2" class="css-checkbox" value="2"  /><label for="s5_20_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s5_20" id="s5_20_3" class="css-checkbox" value="3"  /><label for="s5_20_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>

			
			<div class="submit" id="complete">
						<h3>انتهت المرحلة الخامسة</h3>
						<br/>
						<h2>لقد قمت بالإجابة عل جميع أسئلة المرحلة الخامسة</h2>
						<button type="submit" name="process" class="submit">الانتقال إلى المرحلة السادسة</button>
			</div><!-- end submit step -->
           </center> 
		</div><!-- end middle-wizard -->
		
		</div><!-- end step -->
	</form>
    
</div><!-- end Survey container -->

</section><!-- end section main container -->
      
	  <?php print_footer(); ?>
	  
 <div id="toTop">العودة إلى الأعلى</div>  

</body>
</html>