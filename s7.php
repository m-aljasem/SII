<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="ar">
<?php
	include("func.php");
	print_head();
?>

<body>
<?php print_header(); ?>


                 <div class="container">
             <div class="row">
                 <div class="col-md-12 main-title">
                 <h1>اختبار Strong لتحديد الميول</h1>
                <p>المرحلة السابعة</p>
                </div>
       		</div>
            </div>

<section class="container" id="main">

<div id="survey_container">

	<form name="example-1" id="wrapped" action="chart.php" method="POST" />
					<?php	 for($i=1;$i<136;$i++)
	{
		echo '<input type="hidden" name="s1_'.$i.'" value="'.$_POST["s1_$i"].'" />
		';
	}
	for($i=1;$i<40;$i++)
	{
		echo '<input type="hidden" name="s2_'.$i.'" value="'.$_POST["s2_$i"].'" />
		';
	}
	for($i=1;$i<47;$i++)
	{
		echo '<input type="hidden" name="s3_'.$i.'" value="'.$_POST["s3_$i"].'" />
		';
	}
	for($i=1;$i<30;$i++)
	{
		echo '<input type="hidden" name="s4_'.$i.'" value="'.$_POST["s4_$i"].'" />
		';
	}
		for($i=1;$i<21;$i++)
	{
		echo '<input type="hidden" name="s5_'.$i.'" value="'.$_POST["s5_$i"].'" />
		';
	}
	
	for($i=1;$i<31;$i++)
	{
		echo '<input type="hidden" name="s6_'.$i.'" value="'.$_POST["s6_$i"].'" />
		';
	}
			echo '<input type="hidden" value='.$_POST["user_name"].' name="user_name" />
			';
			echo '<input type="hidden" value='.$_POST["user_age"].' name="user_age" />
			';
			echo '<input type="hidden" value='.$_POST["user_mail"].' name="user_mail" />
			';
			echo '<input type="hidden" value='.$_POST["user_job"].' name="user_job" />
			';
			
	?>
		<div id="middle-wizard">
		<center>	 
			<div class="step row">
					<h3>أَظهِر من فضلكَ هنا أيّ نوعٍ منَ الأشخاص أنت</h3>
					<h3>إذا كانت الحالة تصفك,اختر <u><i>نعم</i></u></h3>
					<h3>إذا كانت الحالة لا تصفك,اختر <u><i>لا</i></u></h3>
					<h3>إذا كُنتَ لا تستطيع أنْ تقرر,اختر <u><i>لا اعرف</i></u></h3>
					<h3>كن صريحا في إجابتك, لأن العلامات المرتفعة والمنخفضة هي علامات هامة جدا لكي تستطيع اختيار المهنة المناسبة </h3>
		
			<hr/>
		
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>﻿أبدأُ النشاطات عادةً في مجموعتي  
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_1" id="s7_1_1" class="css-checkbox" value="1"  /><label for="s7_1_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_1" id="s7_1_2" class="css-checkbox" value="2"  /><label for="s7_1_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_1" id="s7_1_3" class="css-checkbox" value="3"  /><label for="s7_1_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>أُفضِّل العمل وحيداً على العمل مع الجماعة 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_2" id="s7_2_1" class="css-checkbox" value="1"  /><label for="s7_2_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_2" id="s7_2_2" class="css-checkbox" value="2"  /><label for="s7_2_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_2" id="s7_2_3" class="css-checkbox" value="3"  /><label for="s7_2_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>أمتلك مهارةً ميكانيكيةً ( قدرة على الاختراع - الابتكار)
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_3" id="s7_3_1" class="css-checkbox" value="1"  /><label for="s7_3_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_3" id="s7_3_2" class="css-checkbox" value="2"  /><label for="s7_3_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_3" id="s7_3_3" class="css-checkbox" value="3"  /><label for="s7_3_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>أهتمُ بالقضايا الفلسفية مثل الدين ومعنى الحياة 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_4" id="s7_4_1" class="css-checkbox" value="1"  /><label for="s7_4_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_4" id="s7_4_2" class="css-checkbox" value="2"  /><label for="s7_4_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_4" id="s7_4_3" class="css-checkbox" value="3"  /><label for="s7_4_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>يمكنني إعداد إعلانات ناجحة 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_5" id="s7_5_1" class="css-checkbox" value="1"  /><label for="s7_5_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_5" id="s7_5_2" class="css-checkbox" value="2"  /><label for="s7_5_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_5" id="s7_5_3" class="css-checkbox" value="3"  /><label for="s7_5_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>أقوم بتحفيز (تشجيع) طموحات زملائي 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_6" id="s7_6_1" class="css-checkbox" value="1"  /><label for="s7_6_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_6" id="s7_6_2" class="css-checkbox" value="2"  /><label for="s7_6_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_6" id="s7_6_3" class="css-checkbox" value="3"  /><label for="s7_6_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>يمكن أن أكتب تقريراً دقيقاً ومنظّماً بشكلٍ جيّد 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_7" id="s7_7_1" class="css-checkbox" value="1"  /><label for="s7_7_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_7" id="s7_7_2" class="css-checkbox" value="2"  /><label for="s7_7_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_7" id="s7_7_3" class="css-checkbox" value="3"  /><label for="s7_7_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>يمكنني التعامل بسهولةٍ مع الناس ومن مختلف الثقافات 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_8" id="s7_8_1" class="css-checkbox" value="1"  /><label for="s7_8_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_8" id="s7_8_2" class="css-checkbox" value="2"  /><label for="s7_8_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_8" id="s7_8_3" class="css-checkbox" value="3"  /><label for="s7_8_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>أستمتع بالعمل بأدواتٍ يدويةٍ صغيرة 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_9" id="s7_9_1" class="css-checkbox" value="1"  /><label for="s7_9_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_9" id="s7_9_2" class="css-checkbox" value="2"  /><label for="s7_9_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_9" id="s7_9_3" class="css-checkbox" value="3"  /><label for="s7_9_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>يمكنني تهدئة الارتباك والخلافات بين الناس 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_10" id="s7_10_1" class="css-checkbox" value="1"  /><label for="s7_10_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_10" id="s7_10_2" class="css-checkbox" value="2"  /><label for="s7_10_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_10" id="s7_10_3" class="css-checkbox" value="3"  /><label for="s7_10_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>بإمكاني قيادة التنظيمات  
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_11" id="s7_11_1" class="css-checkbox" value="1"  /><label for="s7_11_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_11" id="s7_11_2" class="css-checkbox" value="2"  /><label for="s7_11_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_11" id="s7_11_3" class="css-checkbox" value="3"  /><label for="s7_11_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>أمتلك الصبر عند تدريس الآخرين 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s7_12" id="s7_12_1" class="css-checkbox" value="1"  /><label for="s7_12_1" class="css-label radGroup2">لا</label></td>
		<td><input type="radio" name="s7_12" id="s7_12_2" class="css-checkbox" value="2"  /><label for="s7_12_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s7_12" id="s7_12_3" class="css-checkbox" value="3"  /><label for="s7_12_3" class="css-label radGroup2">نعم</label></td>
				</tr>
				</table>
			
			<hr/>
			<div class="submit " id="complete">
						<h3>انتهت المرحلة السابعة</h3>
						<br/>
						<h2>لقد قمت بالإجابة عل جميع أسئلة المرحلة السابعة والأخيرة</h2>
						<button type="submit" name="process" class="submit">الحصول على نتجة الأختبار</button>
			</div><!-- end submit step -->
			
			</div><!-- end step -->
			
           </center> 
		</div><!-- end middle-wizard -->
	</form>
    
</div><!-- end Survey container -->

</section><!-- end section main container -->
       
	   <?php print_footer(); ?>
	   
 <div id="toTop">العودة إلى الأعلى</div>  

</body>
</html>