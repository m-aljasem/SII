<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="ar">
<?php
	include("func.php");
	print_head();
?>

<body>
<?php print_header(); ?>


       <div class="container">
             <div class="row">
                 <div class="col-md-12 main-title">
                 <h1>اختبار Strong لتحديد الميول</h1>
                <p>المرحلة الثانية</p>
                </div>
       		</div>
            </div>
<section class="container" id="main">
<div id="survey_container">
	<form name="example-1" id="wrapped" action="s3.php" method="POST" />
	<?php 	for($i=1;$i<136;$i++)
	{
		echo '<input type="hidden" name="s1_'.$i.'" value="'.$_POST["s1_$i"].'" />
		';
	}
	
	
			echo '<input type="hidden" value='.$_POST["user_name"].' name="user_name" />
			';
			echo '<input type="hidden" value='.$_POST["user_age"].' name="user_age" />
			';
			echo '<input type="hidden" value='.$_POST["user_mail"].' name="user_mail" />
			';
			echo '<input type="hidden" value='.$_POST["user_job"].' name="user_job" />
			';
		
	?>
		<div id="middle-wizard">
		<center>	 
		<div class="step row">
			<h3>هناكَ بعضُ المواضيع الدراسية مُدرجةٌ هنا.</h3>

<h3> أَظهِر كيف تشعر حول دراسة كل من هذه المواد 
</h3>

				<h3>إذا كنت حقا تعتقد حقا أنك سوف تحب هذه المادة  قم باختيار  <u><i>أحب ذلك</i></u></h3>

				<h3>إذا كنت تعتقد أنك سوف تكون غير مبالي بهذه المادة اختر <u><i>لا أهتم بها</i></u></h3>

				<h3>إذا كنت تعتقد أنك لن تحب هذه المادة اختر <u><i>لا احب ذلك</i></u></h3>

				<h3>ربما ستستمتع ببعض هذه المواضيع حتى ولو لم تكن قد قمتَ بدراستها مسبقاً</h3>
				
			<hr size=30 />
			
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس ﻿التمثيل 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_1" id="s2_1_1" class="css-checkbox" value="1" /><label for="s2_1_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_1" id="s2_1_2" class="css-checkbox" value="2"  /><label for="s2_1_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_1" id="s2_1_3" class="css-checkbox" value="3"  /><label for="s2_1_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة الزراعة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_2" id="s2_2_1" class="css-checkbox" value="1" /><label for="s2_2_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_2" id="s2_2_2" class="css-checkbox" value="2"  /><label for="s2_2_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_2" id="s2_2_3" class="css-checkbox" value="3"  /><label for="s2_2_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس الجبر 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_3" id="s2_3_1" class="css-checkbox" value="1" /><label for="s2_3_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_3" id="s2_3_2" class="css-checkbox" value="2"  /><label for="s2_3_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_3" id="s2_3_3" class="css-checkbox" value="3"  /><label for="s2_3_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة الحساب 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_4" id="s2_4_1" class="css-checkbox" value="1" /><label for="s2_4_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_4" id="s2_4_2" class="css-checkbox" value="2"  /><label for="s2_4_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_4" id="s2_4_3" class="css-checkbox" value="3"  /><label for="s2_4_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة اللغات القديمة مثل اللاتينية, السنسكريتية (لغة هندية قديمة) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_5" id="s2_5_1" class="css-checkbox" value="1" /><label for="s2_5_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_5" id="s2_5_2" class="css-checkbox" value="2"  /><label for="s2_5_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_5" id="s2_5_3" class="css-checkbox" value="3"  /><label for="s2_5_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس الفن 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_6" id="s2_6_1" class="css-checkbox" value="1" /><label for="s2_6_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_6" id="s2_6_2" class="css-checkbox" value="2"  /><label for="s2_6_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_6" id="s2_6_3" class="css-checkbox" value="3"  /><label for="s2_6_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس المحاسبة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_7" id="s2_7_1" class="css-checkbox" value="1" /><label for="s2_7_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_7" id="s2_7_2" class="css-checkbox" value="2"  /><label for="s2_7_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_7" id="s2_7_3" class="css-checkbox" value="3"  /><label for="s2_7_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة علم النبات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_8" id="s2_8_1" class="css-checkbox" value="1" /><label for="s2_8_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_8" id="s2_8_2" class="css-checkbox" value="2"  /><label for="s2_8_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_8" id="s2_8_3" class="css-checkbox" value="3"  /><label for="s2_8_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة التجارة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_9" id="s2_9_1" class="css-checkbox" value="1" /><label for="s2_9_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_9" id="s2_9_2" class="css-checkbox" value="2"  /><label for="s2_9_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_9" id="s2_9_3" class="css-checkbox" value="3"  /><label for="s2_9_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة رياضيات التفاضل والتكامل
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_10" id="s2_10_1" class="css-checkbox" value="1" /><label for="s2_10_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_10" id="s2_10_2" class="css-checkbox" value="2"  /><label for="s2_10_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_10" id="s2_10_3" class="css-checkbox" value="3"  /><label for="s2_10_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة الكيمياء 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_11" id="s2_11_1" class="css-checkbox" value="1" /><label for="s2_11_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_11" id="s2_11_2" class="css-checkbox" value="2"  /><label for="s2_11_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_11" id="s2_11_3" class="css-checkbox" value="3"  /><label for="s2_11_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس التربية الوطنية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_12" id="s2_12_1" class="css-checkbox" value="1" /><label for="s2_12_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_12" id="s2_12_2" class="css-checkbox" value="2"  /><label for="s2_12_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_12" id="s2_12_3" class="css-checkbox" value="3"  /><label for="s2_12_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة علم الكمبيوتر 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_13" id="s2_13_1" class="css-checkbox" value="1" /><label for="s2_13_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_13" id="s2_13_2" class="css-checkbox" value="2"  /><label for="s2_13_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_13" id="s2_13_3" class="css-checkbox" value="3"  /><label for="s2_13_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس علم الاقتصاد 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_14" id="s2_14_1" class="css-checkbox" value="1" /><label for="s2_14_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_14" id="s2_14_2" class="css-checkbox" value="2"  /><label for="s2_14_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_14" id="s2_14_3" class="css-checkbox" value="3"  /><label for="s2_14_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك أن تدرس الكتابة /الإنشاء باللغة العربية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_15" id="s2_15_1" class="css-checkbox" value="1" /><label for="s2_15_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_15" id="s2_15_2" class="css-checkbox" value="2"  /><label for="s2_15_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_15" id="s2_15_3" class="css-checkbox" value="3"  /><label for="s2_15_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس الهندسة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_16" id="s2_16_1" class="css-checkbox" value="1" /><label for="s2_16_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_16" id="s2_16_2" class="css-checkbox" value="2"  /><label for="s2_16_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_16" id="s2_16_3" class="css-checkbox" value="3"  /><label for="s2_16_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة التربية الصحية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_17" id="s2_17_1" class="css-checkbox" value="1" /><label for="s2_17_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_17" id="s2_17_2" class="css-checkbox" value="2"  /><label for="s2_17_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_17" id="s2_17_3" class="css-checkbox" value="3"  /><label for="s2_17_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك أن تدرس الاقتصاد المنزلي 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_18" id="s2_18_1" class="css-checkbox" value="1" /><label for="s2_18_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_18" id="s2_18_2" class="css-checkbox" value="2"  /><label for="s2_18_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_18" id="s2_18_3" class="css-checkbox" value="3"  /><label for="s2_18_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة الفنون الصناعية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_19" id="s2_19_1" class="css-checkbox" value="1" /><label for="s2_19_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_19" id="s2_19_2" class="css-checkbox" value="2"  /><label for="s2_19_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_19" id="s2_19_3" class="css-checkbox" value="3"  /><label for="s2_19_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس الصحافة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_20" id="s2_20_1" class="css-checkbox" value="1" /><label for="s2_20_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_20" id="s2_20_2" class="css-checkbox" value="2"  /><label for="s2_20_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_20" id="s2_20_3" class="css-checkbox" value="3"  /><label for="s2_20_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك أن تدرس الأدب 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_21" id="s2_21_1" class="css-checkbox" value="1" /><label for="s2_21_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_21" id="s2_21_2" class="css-checkbox" value="2"  /><label for="s2_21_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_21" id="s2_21_3" class="css-checkbox" value="3"  /><label for="s2_21_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك أن تدرس الرياضيات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_22" id="s2_22_1" class="css-checkbox" value="1" /><label for="s2_22_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_22" id="s2_22_2" class="css-checkbox" value="2"  /><label for="s2_22_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_22" id="s2_22_3" class="css-checkbox" value="3"  /><label for="s2_22_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك أن تدرس الرسم الميكانيكي ( ذو علاقة بالآلات) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_23" id="s2_23_1" class="css-checkbox" value="1" /><label for="s2_23_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_23" id="s2_23_2" class="css-checkbox" value="2"  /><label for="s2_23_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_23" id="s2_23_3" class="css-checkbox" value="3"  /><label for="s2_23_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس التدريب العسكري
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_24" id="s2_24_1" class="css-checkbox" value="1" /><label for="s2_24_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_24" id="s2_24_2" class="css-checkbox" value="2"  /><label for="s2_24_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_24" id="s2_24_3" class="css-checkbox" value="3"  /><label for="s2_24_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس اللغات المعاصرة ( الحديثة) مثل الفرنسية والألمانية
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_25" id="s2_25_1" class="css-checkbox" value="1" /><label for="s2_25_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_25" id="s2_25_2" class="css-checkbox" value="2"  /><label for="s2_25_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_25" id="s2_25_3" class="css-checkbox" value="3"  /><label for="s2_25_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة دراسة الطبيعة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_26" id="s2_26_1" class="css-checkbox" value="1" /><label for="s2_26_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_26" id="s2_26_2" class="css-checkbox" value="2"  /><label for="s2_26_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_26" id="s2_26_3" class="css-checkbox" value="3"  /><label for="s2_26_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك أن تدرس الخط
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_27" id="s2_27_1" class="css-checkbox" value="1" /><label for="s2_27_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_27" id="s2_27_2" class="css-checkbox" value="2"  /><label for="s2_27_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_27" id="s2_27_3" class="css-checkbox" value="3"  /><label for="s2_27_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس الفلسفة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_28" id="s2_28_1" class="css-checkbox" value="1" /><label for="s2_28_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_28" id="s2_28_2" class="css-checkbox" value="2"  /><label for="s2_28_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_28" id="s2_28_3" class="css-checkbox" value="3"  /><label for="s2_28_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك أن تدرس التربية البدنية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_29" id="s2_29_1" class="css-checkbox" value="1" /><label for="s2_29_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_29" id="s2_29_2" class="css-checkbox" value="2"  /><label for="s2_29_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_29" id="s2_29_3" class="css-checkbox" value="3"  /><label for="s2_29_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة علم الفيزياء 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_30" id="s2_30_1" class="css-checkbox" value="1" /><label for="s2_30_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_30" id="s2_30_2" class="css-checkbox" value="2"  /><label for="s2_30_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_30" id="s2_30_3" class="css-checkbox" value="3"  /><label for="s2_30_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة الفيزيولوجيا ( علم وظائف الأعضاء) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_31" id="s2_31_1" class="css-checkbox" value="1" /><label for="s2_31_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_31" id="s2_31_2" class="css-checkbox" value="2"  /><label for="s2_31_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_31" id="s2_31_3" class="css-checkbox" value="3"  /><label for="s2_31_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس علم السياسة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_32" id="s2_32_1" class="css-checkbox" value="1" /><label for="s2_32_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_32" id="s2_32_2" class="css-checkbox" value="2"  /><label for="s2_32_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_32" id="s2_32_3" class="css-checkbox" value="3"  /><label for="s2_32_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس علم النفس 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_33" id="s2_33_1" class="css-checkbox" value="1" /><label for="s2_33_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_33" id="s2_33_2" class="css-checkbox" value="2"  /><label for="s2_33_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_33" id="s2_33_3" class="css-checkbox" value="3"  /><label for="s2_33_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس الخطابة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_34" id="s2_34_1" class="css-checkbox" value="1" /><label for="s2_34_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_34" id="s2_34_2" class="css-checkbox" value="2"  /><label for="s2_34_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_34" id="s2_34_3" class="css-checkbox" value="3"  /><label for="s2_34_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك أن تدرس الدراسات الدينية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_35" id="s2_35_1" class="css-checkbox" value="1" /><label for="s2_35_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_35" id="s2_35_2" class="css-checkbox" value="2"  /><label for="s2_35_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_35" id="s2_35_3" class="css-checkbox" value="3"  /><label for="s2_35_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب أن تدرس علم الاجتماع 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_36" id="s2_36_1" class="css-checkbox" value="1" /><label for="s2_36_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_36" id="s2_36_2" class="css-checkbox" value="2"  /><label for="s2_36_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_36" id="s2_36_3" class="css-checkbox" value="3"  /><label for="s2_36_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك أن تدرس الإحصاء 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_37" id="s2_37_1" class="css-checkbox" value="1" /><label for="s2_37_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_37" id="s2_37_2" class="css-checkbox" value="2"  /><label for="s2_37_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_37" id="s2_37_3" class="css-checkbox" value="3"  /><label for="s2_37_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل دراسة الطباعة / معالجة النصوص 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_38" id="s2_38_1" class="css-checkbox" value="1" /><label for="s2_38_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_38" id="s2_38_2" class="css-checkbox" value="2"  /><label for="s2_38_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_38" id="s2_38_3" class="css-checkbox" value="3"  /><label for="s2_38_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك أن تدرس علم الحيوان 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s2_39" id="s2_39_1" class="css-checkbox" value="1" /><label for="s2_39_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s2_39" id="s2_39_2" class="css-checkbox" value="2"  /><label for="s2_39_2" class="css-label radGroup2">لا أهتم بها</label></td>
		<td><input type="radio" name="s2_39" id="s2_39_3" class="css-checkbox" value="3"  /><label for="s2_39_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
			

			<div class="submit " id="complete">
						<h3>انتهت المرحلة الثانية</h3>
						<br/>
						<h2>لقد قمت بالإجابة عل جميع أسئلة المرحلة الثانية</h2>
						<button type="submit" name="process" class="submit">الانقال إلى المرحلة الثالثة</button>
			</div><!-- end submit step -->
			
			</div><!-- end step -->
           </center> 
		</div><!-- end middle-wizard -->
        
		
	</form>
    
</div><!-- end Survey container -->

</section><!-- end section main container -->

<?php print_footer(); ?>

 <div id="toTop">العودة إلى الأعلى</div>  

</body>
</html>