<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="ar">
<?php
	include("func.php");
	print_head();
?>

<body>
<?php print_header(); ?>


            <div class="container">
             <div class="row">
                 <div class="col-md-12 main-title">
                 <h1>اختبار Strong لتحديد الميول</h1>
                <p>المرحلة الرابعة</p>
                </div>
       		</div>
            </div>

<section class="container" id="main">

<div id="survey_container">
   	<form name="example-1" id="wrapped" action="s5.php" method="POST" />
			<?php 	for($i=1;$i<136;$i++)
	{
		echo '<input type="hidden" name="s1_'.$i.'" value="'.$_POST["s1_$i"].'" />
		';
	}
	for($i=1;$i<40;$i++)
	{
		echo '<input type="hidden" name="s2_'.$i.'" value="'.$_POST["s2_$i"].'" />
		';
	}
	for($i=1;$i<47;$i++)
	{
		echo '<input type="hidden" name="s3_'.$i.'" value="'.$_POST["s3_$i"].'" />
		';
	}
			echo '<input type="hidden" value='.$_POST["user_name"].' name="user_name" />
			';
			echo '<input type="hidden" value='.$_POST["user_age"].' name="user_age" />
			';
			echo '<input type="hidden" value='.$_POST["user_mail"].' name="user_mail" />
			';
			echo '<input type="hidden" value='.$_POST["user_job"].' name="user_job" />
			'; 
	?>
		<div id="middle-wizard">
		<center>	 
			<div class="step row">
				<h3>أَظهِر كيف تشعر حول هذه الطرق في قضاء وقت فراغك</h3>
				<h3>إذا كنت تحب قضاء وقت فراغك بهذه الطريقةا ختر<u><i>أحب ذلك</i></u></h3>
				<h3>إذا كنت لا تهتم  بقضاء وقت فراغك  بهذه الطريقة قم باختيار<u><i>لا أهتم بها</i></u></h3>
				<h3>إذ كنت لا تحب قضاء وقت فراغك بهذه الطريقة قم باختيار<u><i>لا أحب ذلك</i></u></h3>
				<h3>لا تفكِّر كثيراً بالاحتمالات المختلفة, بل أعطِ الجواب الأوَل الذي يخطر في ذهنك</h3>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في ﻿لعبة الغولف (لعب الكرة بالعصا) 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_1" id="s4_1_1" class="css-checkbox" value="1" /><label for="s4_1_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_1" id="s4_1_2" class="css-checkbox" value="2" /><label for="s4_1_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_1" id="s4_1_3" class="css-checkbox" value="3" /><label for="s4_1_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب حفلات الجاز أو الروك 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_2" id="s4_2_1" class="css-checkbox" value="1" /><label for="s4_2_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_2" id="s4_2_2" class="css-checkbox" value="2"  /><label for="s4_2_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_2" id="s4_2_3" class="css-checkbox" value="3"  /><label for="s4_2_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل الملاكمة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_3" id="s4_3_1" class="css-checkbox" value="1" /><label for="s4_3_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_3" id="s4_3_2" class="css-checkbox" value="2"  /><label for="s4_3_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_3" id="s4_3_3" class="css-checkbox" value="3"  /><label for="s4_3_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في حل الألغاز الميكانيكية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_4" id="s4_4_1" class="css-checkbox" value="1" /><label for="s4_4_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_4" id="s4_4_2" class="css-checkbox" value="2"  /><label for="s4_4_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_4" id="s4_4_3" class="css-checkbox" value="3"  /><label for="s4_4_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التخطيط لحفلة كبيرة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_5" id="s4_5_1" class="css-checkbox" value="1" /><label for="s4_5_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_5" id="s4_5_2" class="css-checkbox" value="2"  /><label for="s4_5_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_5" id="s4_5_3" class="css-checkbox" value="3"  /><label for="s4_5_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل الابتهالات الدينية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_6" id="s4_6_1" class="css-checkbox" value="1" /><label for="s4_6_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_6" id="s4_6_2" class="css-checkbox" value="2"  /><label for="s4_6_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_6" id="s4_6_3" class="css-checkbox" value="3"  /><label for="s4_6_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التدريب في سرية عسكرية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_7" id="s4_7_1" class="css-checkbox" value="1" /><label for="s4_7_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_7" id="s4_7_2" class="css-checkbox" value="2"  /><label for="s4_7_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_7" id="s4_7_3" class="css-checkbox" value="3"  /><label for="s4_7_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل التسوق من أجل آخر الموديلات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_8" id="s4_8_1" class="css-checkbox" value="1" /><label for="s4_8_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_8" id="s4_8_2" class="css-checkbox" value="2"  /><label for="s4_8_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_8" id="s4_8_3" class="css-checkbox" value="3"  /><label for="s4_8_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل حضور الاجتماعات – المؤتمرات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_9" id="s4_9_1" class="css-checkbox" value="1" /><label for="s4_9_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_9" id="s4_9_2" class="css-checkbox" value="2"  /><label for="s4_9_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_9" id="s4_9_3" class="css-checkbox" value="3"  /><label for="s4_9_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في اللعب ضمن فريق رياضي مع الأصدقاء
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_10" id="s4_10_1" class="css-checkbox" value="1" /><label for="s4_10_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_10" id="s4_10_2" class="css-checkbox" value="2"  /><label for="s4_10_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_10" id="s4_10_3" class="css-checkbox" value="3"  /><label for="s4_10_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب العمل لصالح مرشح لمنصب مُعَيَّن
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_11" id="s4_11_1" class="css-checkbox" value="1" /><label for="s4_11_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_11" id="s4_11_2" class="css-checkbox" value="2"  /><label for="s4_11_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_11" id="s4_11_3" class="css-checkbox" value="3"  /><label for="s4_11_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في المعارِض الفنّية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_12" id="s4_12_1" class="css-checkbox" value="1" /><label for="s4_12_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_12" id="s4_12_2" class="css-checkbox" value="2"  /><label for="s4_12_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_12" id="s4_12_3" class="css-checkbox" value="3"  /><label for="s4_12_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب كتابة مسرحية بفصل واحد 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_13" id="s4_13_1" class="css-checkbox" value="1" /><label for="s4_13_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_13" id="s4_13_2" class="css-checkbox" value="2"  /><label for="s4_13_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_13" id="s4_13_3" class="css-checkbox" value="3"  /><label for="s4_13_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في قائد اوركسترا 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_14" id="s4_14_1" class="css-checkbox" value="1" /><label for="s4_14_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_14" id="s4_14_2" class="css-checkbox" value="2"  /><label for="s4_14_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_14" id="s4_14_3" class="css-checkbox" value="3"  /><label for="s4_14_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب العمل ضمن جماعة دينية شبابية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_15" id="s4_15_1" class="css-checkbox" value="1" /><label for="s4_15_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_15" id="s4_15_2" class="css-checkbox" value="2"  /><label for="s4_15_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_15" id="s4_15_3" class="css-checkbox" value="3"  /><label for="s4_15_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل قراءة الصفحات الرياضية في المجلات
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_16" id="s4_16_1" class="css-checkbox" value="1" /><label for="s4_16_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_16" id="s4_16_2" class="css-checkbox" value="2"  /><label for="s4_16_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_16" id="s4_16_3" class="css-checkbox" value="3"  /><label for="s4_16_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في نَظْم الشِّعْر 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_17" id="s4_17_1" class="css-checkbox" value="1" /><label for="s4_17_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_17" id="s4_17_2" class="css-checkbox" value="2"  /><label for="s4_17_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_17" id="s4_17_3" class="css-checkbox" value="3"  /><label for="s4_17_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في التَّزلُّج 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_18" id="s4_18_1" class="css-checkbox" value="1" /><label for="s4_18_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_18" id="s4_18_2" class="css-checkbox" value="2"  /><label for="s4_18_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_18" id="s4_18_3" class="css-checkbox" value="3"  /><label for="s4_18_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في قراءة المجلات التجارية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_19" id="s4_19_1" class="css-checkbox" value="1" /><label for="s4_19_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_19" id="s4_19_2" class="css-checkbox" value="2"  /><label for="s4_19_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_19" id="s4_19_3" class="css-checkbox" value="3"  /><label for="s4_19_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل قراءة المجلات الميكانيكية الشعبية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_20" id="s4_20_1" class="css-checkbox" value="1" /><label for="s4_20_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_20" id="s4_20_2" class="css-checkbox" value="2"  /><label for="s4_20_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_20" id="s4_20_3" class="css-checkbox" value="3"  /><label for="s4_20_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب قراءة مجلات حول الفن أو الموسيقى 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_21" id="s4_21_1" class="css-checkbox" value="1" /><label for="s4_21_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_21" id="s4_21_2" class="css-checkbox" value="2"  /><label for="s4_21_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_21" id="s4_21_3" class="css-checkbox" value="3"  /><label for="s4_21_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في حضور المحاضرات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_22" id="s4_22_1" class="css-checkbox" value="1" /><label for="s4_22_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_22" id="s4_22_2" class="css-checkbox" value="2"  /><label for="s4_22_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_22" id="s4_22_3" class="css-checkbox" value="3"  /><label for="s4_22_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل إنجاز التجارب العلمية 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_23" id="s4_23_1" class="css-checkbox" value="1" /><label for="s4_23_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_23" id="s4_23_2" class="css-checkbox" value="2"  /><label for="s4_23_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_23" id="s4_23_3" class="css-checkbox" value="3"  /><label for="s4_23_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تحب التَّخييم 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_24" id="s4_24_1" class="css-checkbox" value="1" /><label for="s4_24_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_24" id="s4_24_2" class="css-checkbox" value="2"  /><label for="s4_24_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_24" id="s4_24_3" class="css-checkbox" value="3"  /><label for="s4_24_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل إعداد العشاء للضيوف 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_25" id="s4_25_1" class="css-checkbox" value="1" /><label for="s4_25_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_25" id="s4_25_2" class="css-checkbox" value="2"  /><label for="s4_25_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_25" id="s4_25_3" class="css-checkbox" value="3"  /><label for="s4_25_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل تسلية الآخرين 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_26" id="s4_26_1" class="css-checkbox" value="1" /><label for="s4_26_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_26" id="s4_26_2" class="css-checkbox" value="2"  /><label for="s4_26_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_26" id="s4_26_3" class="css-checkbox" value="3"  /><label for="s4_26_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في تجريب وصفات طبخ جديدة 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_27" id="s4_27_1" class="css-checkbox" value="1" /><label for="s4_27_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_27" id="s4_27_2" class="css-checkbox" value="2"  /><label for="s4_27_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_27" id="s4_27_3" class="css-checkbox" value="3"  /><label for="s4_27_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>هل تفضل أن أكون أوّل من يلبس آخر الأزياء 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_28" id="s4_28_1" class="css-checkbox" value="1" /><label for="s4_28_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_28" id="s4_28_2" class="css-checkbox" value="2"  /><label for="s4_28_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_28" id="s4_28_3" class="css-checkbox" value="3"  /><label for="s4_28_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>ما رأيك في تنظيم عروض المسرحيات 
؟</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s4_29" id="s4_29_1" class="css-checkbox" value="1" /><label for="s4_29_1" class="css-label radGroup2">لا أحب ذلك</label></td>
		<td><input type="radio" name="s4_29" id="s4_29_2" class="css-checkbox" value="2"  /><label for="s4_29_2" class="css-label radGroup2">لا أعرف</label></td>
		<td><input type="radio" name="s4_29" id="s4_29_3" class="css-checkbox" value="3"  /><label for="s4_29_3" class="css-label radGroup2">أحب ذلك</label></td>
				</tr>
				</table>
		

			<div class="submit" id="complete">
						<h3>انتهت المرحلة الرابعة</h3>
						<br/>
						<h2>لقد قمت بالإجابة عل جميع أسئلة المرحلة الرابعة</h2>
						<button type="submit" name="process" class="submit">الانتقال إلى المرحلة الخامسة</button>
			</div><!-- end submit step -->
			
		</div><!-- end step -->
           </center> 
		</div><!-- end middle-wizard -->
	</form>
    
</div><!-- end Survey container -->

</section><!-- end section main container -->
       <?php print_footer(); ?>
 <div id="toTop">العودة إلى الأعلى</div>  
</body>
</html>