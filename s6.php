<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="ar">
<?php
	include("func.php");
	print_head();
?>

<body>
<?php print_header(); ?>


            <div class="container">
             <div class="row">
                 <div class="col-md-12 main-title">
                 <h1>اختبار Strong لتحديد الميول</h1>
                <p>المرحلة  السادسة</p>
                </div>
       		</div>
            </div>

<section class="container" id="main">

<div id="survey_container">

    
	<form name="example-1" id="wrapped" action="s7.php" method="POST" />
		<?php	for($i=1;$i<136;$i++)
	{
		echo '<input type="hidden" name="s1_'.$i.'" value="'.$_POST["s1_$i"].'" />
		';
	}
	for($i=1;$i<40;$i++)
	{
		echo '<input type="hidden" name="s2_'.$i.'" value="'.$_POST["s2_$i"].'" />
		';
	}
	for($i=1;$i<47;$i++)
	{
		echo '<input type="hidden" name="s3_'.$i.'" value="'.$_POST["s3_$i"].'" />
		';
	}
	for($i=1;$i<30;$i++)
	{
		echo '<input type="hidden" name="s4_'.$i.'" value="'.$_POST["s4_$i"].'" />
		';
	}
	for($i=1;$i<21;$i++)
	{
		echo '<input type="hidden" name="s5_'.$i.'" value="'.$_POST["s5_$i"].'" />
		';
	}
			echo '<input type="hidden" value='.$_POST["user_name"].' name="user_name" />
			';
			echo '<input type="hidden" value='.$_POST["user_age"].' name="user_age" />
			';
			echo '<input type="hidden" value='.$_POST["user_mail"].' name="user_mail" />
			';
			echo '<input type="hidden" value='.$_POST["user_job"].' name="user_job" />
			'; 
	?>
		<div id="middle-wizard">
		<center>	 
			
					<div class="step row">
						<h3>يوجد هنا مجموعة أزواج من النشاطات أو المهن أظهِر أيّ واحد من الزوجين تفضِّل أكثر</h3>
						<h3>في حال كنتَ تفضّل النشاط الأوّل من كل زوج اختر <u><i>النشاط الأول </i></u></h3>
						<h3>في حال كنتَ تفضّل النشاط الثاني من كل زوج اختر <u><i>النشاط الثاني</i></u></h3>
						<h3>في حال كنت تحبّ كلا النشاطين, أو في حال كنت لا تحبّ أيّاً من النشاطين, أو في حال كنت لا تستطيع أن تقرّر أيّاً من النشاطين تفضِّل اختر <u><i>محايد</i></u></h3>
						<h3>الرجاء أن تضع إشارة واحدة فقط لكل زوج من هذه الأزواج</h3>
					
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : ﻿كابتن طيارة 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : ﻿مندوب بيع بطاقات طيران
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_1" id="s6_1_1" class="css-checkbox" value="1"  /><label for="s6_1_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_1" id="s6_1_2" class="css-checkbox" value="2"  /><label for="s6_1_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_1" id="s6_1_3" class="css-checkbox" value="3"  /><label for="s6_1_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : سائق سيارة أجرة 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : ضابط شرطة
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_2" id="s6_2_1" class="css-checkbox" value="1"  /><label for="s6_2_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_2" id="s6_2_2" class="css-checkbox" value="2"  /><label for="s6_2_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_2" id="s6_2_3" class="css-checkbox" value="3"  /><label for="s6_2_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : رئيس النادلين 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : رب المنزل 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_3" id="s6_3_1" class="css-checkbox" value="1"  /><label for="s6_3_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_3" id="s6_3_2" class="css-checkbox" value="2"  /><label for="s6_3_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_3" id="s6_3_3" class="css-checkbox" value="3"  /><label for="s6_3_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : تطوير الخطط 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : تنفيذ الخطط 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_4" id="s6_4_1" class="css-checkbox" value="1"  /><label for="s6_4_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_4" id="s6_4_2" class="css-checkbox" value="2"  /><label for="s6_4_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_4" id="s6_4_3" class="css-checkbox" value="3"  /><label for="s6_4_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : أن تقوم بأعمالك بنفسك 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : أن تطلب من أحدٍ ما كي يقوم بالعمل
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_5" id="s6_5_1" class="css-checkbox" value="1"  /><label for="s6_5_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_5" id="s6_5_2" class="css-checkbox" value="2"  /><label for="s6_5_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_5" id="s6_5_3" class="css-checkbox" value="3"  /><label for="s6_5_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : التعامل مع الأشياء 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : التعامل مع الناس
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_6" id="s6_6_1" class="css-checkbox" value="1"  /><label for="s6_6_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_6" id="s6_6_2" class="css-checkbox" value="2"  /><label for="s6_6_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_6" id="s6_6_3" class="css-checkbox" value="3"  /><label for="s6_6_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : العمل بدوام كامل  
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : العمل بدوام جزئي
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_7" id="s6_7_1" class="css-checkbox" value="1"  /><label for="s6_7_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_7" id="s6_7_2" class="css-checkbox" value="2"  /><label for="s6_7_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_7" id="s6_7_3" class="css-checkbox" value="3"  /><label for="s6_7_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : تجريب حظك / المخاطرة
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : القيام بأعمالٍ آمنة 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_8" id="s6_8_1" class="css-checkbox" value="1"  /><label for="s6_8_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_8" id="s6_8_2" class="css-checkbox" value="2"  /><label for="s6_8_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_8" id="s6_8_3" class="css-checkbox" value="3"  /><label for="s6_8_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : استلام مرتب محدد 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : تقاضي مبلغاً مقابل كل ما يتمّ عمله
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_9" id="s6_9_1" class="css-checkbox" value="1"  /><label for="s6_9_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_9" id="s6_9_2" class="css-checkbox" value="2"  /><label for="s6_9_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_9" id="s6_9_3" class="css-checkbox" value="3"  /><label for="s6_9_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : العمل الخارجي (خارج المنزل)
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : العمل الداخلي (داخل المنزل)
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_10" id="s6_10_1" class="css-checkbox" value="1"  /><label for="s6_10_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_10" id="s6_10_2" class="css-checkbox" value="2"  /><label for="s6_10_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_10" id="s6_10_3" class="css-checkbox" value="3"  /><label for="s6_10_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : عمل مستقل ( شخصي) 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : تنفيذ برنامج محدّد من قبِل مشرف تحترمه 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_11" id="s6_11_1" class="css-checkbox" value="1"  /><label for="s6_11_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_11" id="s6_11_2" class="css-checkbox" value="2"  /><label for="s6_11_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_11" id="s6_11_3" class="css-checkbox" value="3"  /><label for="s6_11_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : راقب في مستشفى 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : حارس في السجن
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_12" id="s6_12_1" class="css-checkbox" value="1"  /><label for="s6_12_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_12" id="s6_12_2" class="css-checkbox" value="2"  /><label for="s6_12_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_12" id="s6_12_3" class="css-checkbox" value="3"  /><label for="s6_12_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : مُستشار – مُرشِد مهني 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : موظّف الصحة العامة
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_13" id="s6_13_1" class="css-checkbox" value="1"  /><label for="s6_13_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_13" id="s6_13_2" class="css-checkbox" value="2"  /><label for="s6_13_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_13" id="s6_13_3" class="css-checkbox" value="3"  /><label for="s6_13_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : أن تكون صديق باحث علمي 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : أن تكون صديق لمدير مبيعات
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_14" id="s6_14_1" class="css-checkbox" value="1"  /><label for="s6_14_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_14" id="s6_14_2" class="css-checkbox" value="2"  /><label for="s6_14_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_14" id="s6_14_3" class="css-checkbox" value="3"  /><label for="s6_14_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : النشاط الجسدي
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : النشاط العقلي
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_15" id="s6_15_1" class="css-checkbox" value="1"  /><label for="s6_15_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_15" id="s6_15_2" class="css-checkbox" value="2"  /><label for="s6_15_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_15" id="s6_15_3" class="css-checkbox" value="3"  /><label for="s6_15_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : النشاطات المثيرة و المحفوفة بالمخاطر
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : النشاطات الآمنة والهادئة
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_16" id="s6_16_1" class="css-checkbox" value="1"  /><label for="s6_16_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_16" id="s6_16_2" class="css-checkbox" value="2"  /><label for="s6_16_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_16" id="s6_16_3" class="css-checkbox" value="3"  /><label for="s6_16_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : مدرِّب تربية بدنية
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : كاتب حرّ ( طليق بدون قيود)
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_17" id="s6_17_1" class="css-checkbox" value="1"  /><label for="s6_17_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_17" id="s6_17_2" class="css-checkbox" value="2"  /><label for="s6_17_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_17" id="s6_17_3" class="css-checkbox" value="3"  /><label for="s6_17_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : خبير إحصاء  
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : عامل في مجال اجتماعي
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_18" id="s6_18_1" class="css-checkbox" value="1"  /><label for="s6_18_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_18" id="s6_18_2" class="css-checkbox" value="2"  /><label for="s6_18_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_18" id="s6_18_3" class="css-checkbox" value="3"  /><label for="s6_18_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : مسؤولية تقنية ( فنية ) ( مسؤول عن 25 من  العاملين في مجال علمي) 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : مسؤولية إشرافية ( توجيهية) كأن تكون مسؤولاً عن 300 عامل في أعمال مكتبية
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_19" id="s6_19_1" class="css-checkbox" value="1"  /><label for="s6_19_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_19" id="s6_19_2" class="css-checkbox" value="2"  /><label for="s6_19_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_19" id="s6_19_3" class="css-checkbox" value="3"  /><label for="s6_19_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : الذهاب لحضور مسرحية 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : الذهاب للرقص
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_20" id="s6_20_1" class="css-checkbox" value="1"  /><label for="s6_20_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_20" id="s6_20_2" class="css-checkbox" value="2"  /><label for="s6_20_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_20" id="s6_20_3" class="css-checkbox" value="3"  /><label for="s6_20_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : معلِّم
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : بائع
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_21" id="s6_21_1" class="css-checkbox" value="1"  /><label for="s6_21_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_21" id="s6_21_2" class="css-checkbox" value="2"  /><label for="s6_21_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_21" id="s6_21_3" class="css-checkbox" value="3"  /><label for="s6_21_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : تجريب مستحضرات تجميل جديدة 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : التجربة مع تجهيزات مكتب جديدة 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_22" id="s6_22_1" class="css-checkbox" value="1"  /><label for="s6_22_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_22" id="s6_22_2" class="css-checkbox" value="2"  /><label for="s6_22_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_22" id="s6_22_3" class="css-checkbox" value="3"  /><label for="s6_22_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : أن تكون مسؤولاً عن جني المال لمساعدة عائلتك 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : أن تكون مسؤولا عن الاهتمام بالأطفال
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_23" id="s6_23_1" class="css-checkbox" value="1"  /><label for="s6_23_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_23" id="s6_23_2" class="css-checkbox" value="2"  /><label for="s6_23_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_23" id="s6_23_3" class="css-checkbox" value="3"  /><label for="s6_23_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : العمل في شركة كبيرة مع فرصة صغيرة لأن    تصبح رئيساً للشركة في عمر 55 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : العمل بشكل مستقل في مشروع صغير 
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_24" id="s6_24_1" class="css-checkbox" value="1"  /><label for="s6_24_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_24" id="s6_24_2" class="css-checkbox" value="2"  /><label for="s6_24_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_24" id="s6_24_3" class="css-checkbox" value="3"  /><label for="s6_24_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : العمل بالاستيراد والتصدير  
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : العمل في مختبر للبحث
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_25" id="s6_25_1" class="css-checkbox" value="1"  /><label for="s6_25_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_25" id="s6_25_2" class="css-checkbox" value="2"  /><label for="s6_25_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_25" id="s6_25_3" class="css-checkbox" value="3"  /><label for="s6_25_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : أحداث الموسيقى والفنون 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : الأحداث الرياضية
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_26" id="s6_26_1" class="css-checkbox" value="1"  /><label for="s6_26_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_26" id="s6_26_2" class="css-checkbox" value="2"  /><label for="s6_26_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_26" id="s6_26_3" class="css-checkbox" value="3"  /><label for="s6_26_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : قراءة الكتب
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : مشاهدة التلفاز والذهاب لمشاهدة السينما
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_27" id="s6_27_1" class="css-checkbox" value="1"  /><label for="s6_27_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_27" id="s6_27_2" class="css-checkbox" value="2"  /><label for="s6_27_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_27" id="s6_27_3" class="css-checkbox" value="3"  /><label for="s6_27_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
			<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : تقدير أسعار العقارات أو الأبنية  
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : إصلاح و إعادة ترميم قطع قديمة
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_28" id="s6_28_1" class="css-checkbox" value="1"  /><label for="s6_28_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_28" id="s6_28_2" class="css-checkbox" value="2"  /><label for="s6_28_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_28" id="s6_28_3" class="css-checkbox" value="3"  /><label for="s6_28_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : العمل مع منظمات غير ربحية /خيرية/  
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : العمل مع منظمات ذات توجه ربحي
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_29" id="s6_29_1" class="css-checkbox" value="1"  /><label for="s6_29_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_29" id="s6_29_2" class="css-checkbox" value="2"  /><label for="s6_29_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_29" id="s6_29_3" class="css-checkbox" value="3"  /><label for="s6_29_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
				<hr/>
				<table>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الأول : عمل يتطلّب التنقّل من مكان لآخر 
</h3></td>
				</tr>
				<tr class="tab_header" border>
						<td colspan="3"><h3>النشاط الثاني : العمل حيث تعيش في مكان واحد
</h3></td>
				</tr>
				<tr>
		<td><input type="radio" name="s6_30" id="s6_30_1" class="css-checkbox" value="1"  /><label for="s6_30_1" class="css-label radGroup2">النشاط الثاني</label></td>
		<td><input type="radio" name="s6_30" id="s6_30_2" class="css-checkbox" value="2"  /><label for="s6_30_2" class="css-label radGroup2">محايد</label></td>
		<td><input type="radio" name="s6_30" id="s6_30_3" class="css-checkbox" value="3"  /><label for="s6_30_3" class="css-label radGroup2">النشاط الأول</label></td>
				</tr>
				</table>
			
				<hr/>
			<div class="submit" id="complete">
						<h3>انتهت المرحلة السادسة</h3>
						<br/>
						<h2>لقد قمت بالإجابة عل جميع أسئلة المرحلة السادسة</h2>
						<button type="submit" name="process" class="submit">الانتقال إلى المرحلة السابعة</button>
			</div><!-- end submit step -->
			
			</div><!-- end step -->
           </center> 
		</div><!-- end middle-wizard -->
        
	</form>
    
</div><!-- end Survey container -->

</section><!-- end section main container -->
 
 <?php print_footer(); ?>
 
 <div id="toTop">العودة إلى الأعلى</div>  
</body>
</html>